#ifndef __YUSUR_MM_H
#define __YUSUR_MM_H

#define MM_NAME_SIZE		16

#undef pr_debug
#define pr_debug pr_info

struct mm_region {
	uint64_t address;
	uint64_t size;
	struct mm_region *next;
};

struct yusur_mm {
	const char *name;
	uint64_t base_address;
	uint64_t total_size;
	uint64_t align_mask;
	struct mm_region *used_list;
	struct mm_region *free_list;
};

#ifdef MM_VERBOSE_DEBUG
static inline void yusur_mm_dump(struct yusur_mm *ymm)
{
	struct mm_region *mr;
	int i;

	pr_debug("Memory Manage %s used list:\n", ymm->name);

	for (i = 1, mr = ymm->used_list; mr; i++, mr = mr->next)
		pr_debug("[%d] 0x%llx+0x%llx ->\n", i, mr->address, mr->size);

	pr_debug("Memory Manage %s freed list:\n", ymm->name);

	for (i = 1, mr = ymm->free_list; mr; i++, mr = mr->next)
		pr_debug("[%d] 0x%llx+0x%llx ->\n", i, mr->address, mr->size);
}
#else
static inline void yusur_mm_dump(struct yusur_mm *ymm) {}
#endif /* MM_VERBOSE_DEBUG */

struct yusur_mm *yusur_mm_init(uint64_t base, uint64_t size,
			       uint32_t align_mask, const char *name);
void yusur_mm_uninit(struct yusur_mm *ymm);

/**
 * Alloc memory from user mm_pool
 *
 * @param ymm: memory poll
 * @param size: size of the memory region to be requested
 * @return -ENOMEM: fail to alloc. >=0: address of the memory region
 */
int64_t yusur_malloc(struct yusur_mm *ymm, uint64_t size);
void yusur_free(struct yusur_mm *ymm, uint64_t address);

#endif /* __YUSUR_MM_H */
