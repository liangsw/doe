#ifndef __YUSDOE_H_
#define __YUSDOE_H_

#include "include/uapi_yusdoe.h"
#include "yusur_mm.h"
#include "yusdoe_reg.h"

/* cmd buffer define */
#define YUSDOE_CB_MAX_CMD			1000

#define YUSDOE_WRITE_CMD_BUFFER_SIZE		65536
#define YUSDOE_WRITE_CMD_BUFFER_CNT		16
#define YUSDOE_WRITE_EVENTQ_DEPTH		16384
#define YUSDOE_WRITE_EVENTQ_ENTRY_SIZE		256

#define YUSDOE_READ_CMD_BUFFER_SIZE		65536
#define YUSDOE_READ_CMD_BUFFER_CNT		16
#define YUSDOE_READ_EVENTQ_DEPTH		16384
#define YUSDOE_READ_EVENTQ_ENTRY_SIZE		256

#define YUSDOE_CMD_TIMEOUT			(20*HZ)		/* 20s */
#define POLLING_TIMEDOUT			100000000ULL	/* 100ms */
#define POLLING_WORK_SCHEDULE_TIME		10000000000ULL	/* 10s */

struct yusdoe_event_queue {
	/* buffer */
	dma_addr_t dma_base;
	void *base;
	uint32_t depth;
	uint32_t entry_size;
	uint32_t entry_bit;
	/* pointer */
	dma_addr_t dma_hw_tail;
	void *hw_tail_ptr;
	uint32_t sw_head;
	uint32_t sw_tail;
};

#define EQ_WITHDRAW_HEAD(eq)	(eq)->sw_head = ((eq)->sw_head - 1) &		\
						((eq)->depth - 1)
#define EQ_MOVE_HEAD(eq)	(eq)->sw_head = ((eq)->sw_head + 1) &		\
						((eq)->depth - 1)
#define EQ_MOVE_TAIL(eq)	(eq)->sw_tail = ((eq)->sw_tail + 1) &		\
						((eq)->depth - 1)
#define EQ_IS_FULL(eq)		((((eq)->sw_head + 1) & ((eq)->depth - 1)) ==	\
				 (eq)->sw_tail)

#define EQ_HW_TAIL(eq)		((le64_to_cpu(*(uint64_t *)(eq)->hw_tail_ptr) -	\
				 (uint64_t)(eq)->base) >> (eq)->entry_bit)
#define EQ_AVAILABLE(eq)	((EQ_HW_TAIL(eq) + (eq)->depth - (eq)->sw_tail) &\
				 ((eq)->depth - 1))

#define CB_MOVE_HEAD(doe_if)	(doe_if)->cb_head = ((doe_if)->cb_head + 1) &	\
						     ((doe_if)->cb_depth - 1);
#define CB_MOVE_TAIL(doe_if)	(doe_if)->cb_tail = ((doe_if)->cb_tail + 1) &	\
						     ((doe_if)->cb_depth - 1);
#define CB_IS_FULL(doe_if)	((((doe_if)->cb_tail + 1) & ((doe_if)->cb_depth - 1)) ==\
				 (doe_if)->cb_head)
#define CB_ALMOST_FULL(doe_if)	((((doe_if)->cb_tail + 2) & ((doe_if)->cb_depth - 1)) ==\
				 (doe_if)->cb_head)

struct yusdoe_cmd_buffer {
	dma_addr_t dma_base;
	void *base;
	uint32_t size;
	uint32_t ptr;
	uint32_t cmd_cnt;
};

struct yusdoe_interface {
	struct yusdoe_device		*yusdoe;
	const char			*name;
	uint8_t				msi_index;
	void __iomem			*dma_reg_base;
	struct yusdoe_cmd_buffer	*cb;
	uint8_t				cb_head;
	uint8_t				cb_tail;
	uint8_t				cb_depth;
	union {
		uint16_t		cmd_tag;
		struct {
			uint16_t	cmd_id : 15;
			uint16_t	is_read : 1;
		};
	};
	struct yusdoe_event_queue	eq;
	struct work_struct		work;
	struct llist_head		pending_llist;
	struct list_head		work_list;
	struct list_head		cache_list;
	/*
	 * Both user ioctl thread and desc-complete thread will access
	 * the command pointer. We must avoid the access to command
	 * pointer after freed.
	 * Most of the time lock contention does not occur unless timeout.
	 */
	spinlock_t			work_lock;
};

struct yusdoe_desc {
	struct yusdoe_device		*yusdoe;
	struct yusdoe_sw_cmd		*parent;
	struct yusdoe_sw_cmd		*cmd;
	/* Do nothing when invalid set */
	uint8_t				invalid;
	union {
		uint16_t		cmd_tag;
		struct {
			uint16_t	cmd_id : 15;
			uint16_t	is_read : 1;
		};
	};
	struct llist_node		llnode;
	struct list_head		list;
	struct list_head		cache;
	int (*complete_desc)(struct yusdoe_desc *, struct yusdoe_event *);
};

struct yusdoe_table_spec {
	struct yusdoe_miu_param		miu_param;
	struct yusdoe_hie_param		hie_param;
	struct yusdoe_aie_param		aie_param;
	struct yusdoe_index_param	index_param;
	struct yusdoe_cache_config	l1_cache[YUSDOE_L1CACHE_NUM];
	struct yusdoe_cache_config	l2_cache;
};

struct yusdoe_device {
	struct device			*pdev;

	/* char device */
	struct list_head		list;
	struct cdev			cdev;
	struct device			*dev;
	dev_t 				devt;

	/* interface */
	wait_queue_head_t		wait;
	void __iomem			*doe_base;	/* doe reg */
	struct yusdoe_interface		*doe_read_if;
	struct yusdoe_interface		*doe_write_if;
	struct mutex			mutex;

	/* hardware resources */
	struct yusdoe_table_param	param[YUSDOE_TBL_NUM];
	struct yusdoe_table_spec	spec[YUSDOE_USER_TBL_NUM + 2];
	struct yusur_mm			*ddr0_hash;
	struct yusur_mm			*ddr1_array;
	struct yusur_mm			*l1_cache[YUSDOE_L1CACHE_NUM];
	struct yusur_mm			*l2_cache;
	struct yusur_mm			*index_sram;
	unsigned long			*tbl_bitmap;
	struct yusur_mm			*l1_tag[YUSDOE_L1CACHE_NUM];
	struct yusur_mm			*l2_tag;
};

static inline void yusdoe_writel(struct yusdoe_device *yusdoe, uint32_t val,
						 volatile void __iomem *addr)
{
	uint32_t old = readl(addr);

	writel(val, addr);
	dev_dbg(yusdoe->dev, "write 0x%08x to 0x%px : 0x%08x -> 0x%08x\n",
			     val, addr, old, readl(addr));
}

#ifdef DOE_VERBOSE_DEBUG
static inline void buffer_dump(struct yusdoe_device *yusdoe, const char *name,
					void *addr, uint32_t size)
{
	char buf[128];
	int i, offset;

	dev_dbg(yusdoe->dev, "DUMP %s form 0x%px\n", name, addr);

	for (i = 0; i < size; i++) {
		if (!(i % 16)) {
			sprintf(buf, "%04x: ", i);
			offset = 6;
		}

		sprintf(buf + offset, "%02x ", *(uint8_t *)(addr + i));
		offset += 3;

		if (i % 16 == 15 || i + 1 == size)
			dev_dbg(yusdoe->dev, "%s\n", buf);
	}
}
#else
static inline void buffer_dump(struct yusdoe_device *yusdoe, const char *name,
					void *addr, uint32_t size) {}
#endif /* DOE_VERBOSE_DEBUG */

static inline uint8_t yusdoe_get_order(uint32_t len)
{
	uint8_t order = 0;

	while ((1 << order) < len)
		order++;

	return order;
}

/* yusdoe_core.c */
int yusdoe_hcu_init(struct yusdoe_device *yusdoe);
int yusdoe_submit_ddr_spec(struct yusdoe_device *yusdoe, uint8_t spec_tbl,
			   struct yusdoe_sw_cmd *parent,
			   int (*complete_desc)(struct yusdoe_desc *,
						struct yusdoe_event *));
int yusdoe_process_cmd(struct yusdoe_device *yusdoe, struct yusdoe_sw_cmd *cmd);
int yusdoe_enqueue_cmdbuffer(struct yusdoe_interface *doe_if,
			     struct yusdoe_desc *desc);
struct yusdoe_sw_cmd *yusdoe_sw_cmd_prepare(struct yusdoe_device *yusdoe,
					    unsigned long arg);
void yusdoe_sw_cmd_unprepare(struct yusdoe_device *yusdoe,
			     struct yusdoe_sw_cmd *sw_cmd);

/* yusdoe_schedule.c */
int yusdoe_user_cmd_context(struct yusdoe_device *yusdoe,
			    struct yusdoe_sw_cmd *cmd);
int yusdoe_submit_cmd(struct yusdoe_device *yusdoe, struct yusdoe_sw_cmd *cmd,
		      struct yusdoe_sw_cmd *parent,
		      int (*complete_desc)(struct yusdoe_desc *,
					   struct yusdoe_event *));
void yusdoe_polling_work(struct work_struct *work);

/* yusdoe_if.c */
int yusdoe_reg_init(struct yusdoe_device *yusdoe);
int yusdoe_dma_busy(struct yusdoe_interface *doe_if);
int yusdoe_send_cmd(struct yusdoe_interface *doe_if);
int yusdoe_check_irq(struct yusdoe_interface *doe_if);
void yusdoe_clean_irq(struct yusdoe_interface *doe_if);

#endif /* __YUSDOE_H_ */
