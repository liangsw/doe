#include <linux/cdev.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/spinlock.h>
#include <linux/uaccess.h>
#include <linux/workqueue.h>

#include "yusdoe.h"

static struct class *yusdoe_class;
static dev_t yusdoe_devt;
static LIST_HEAD(yusdoe_list);

static const struct pci_device_id yusdoe_dev_ids[] = {
	{ PCI_DEVICE(0x1556, 0x1111), },
	{ PCI_DEVICE(0x10ee, 0x9338), },
	{ 0, },
};

struct yusdoe_interface *yusdoe_alloc_interface(struct yusdoe_device *yusdoe,
						const char *name,
						uint32_t cmd_buffer_size,
						uint8_t cmd_buffer_cnt,
						uint8_t msi_index,
						uint32_t eq_entry_size,
						uint32_t eq_depth)
{
	struct device *dev = yusdoe->pdev;
	struct yusdoe_interface *doe_if;
	struct yusdoe_cmd_buffer *cb;
	struct yusdoe_event_queue *eq;
	int i;

	/* alloc interface structure */
	doe_if = devm_kzalloc(dev, sizeof(*doe_if), GFP_KERNEL);
	if (!doe_if)
		return ERR_PTR(-ENOMEM);

	INIT_LIST_HEAD(&doe_if->work_list);
	INIT_LIST_HEAD(&doe_if->cache_list);
	init_llist_head(&doe_if->pending_llist);
	spin_lock_init(&doe_if->work_lock);
	doe_if->name = name;
	doe_if->msi_index = msi_index;
	doe_if->yusdoe = yusdoe;
	INIT_WORK(&doe_if->work, yusdoe_polling_work);

	/* alloc cmd buffer */
	doe_if->cb_depth = cmd_buffer_cnt;
	doe_if->cb = devm_kzalloc(dev, sizeof(*doe_if->cb) * cmd_buffer_cnt,
				       GFP_KERNEL);
	if (!doe_if->cb)
		return ERR_PTR(-ENOMEM);

	for (i = 0; i < cmd_buffer_cnt; i++) {
		cb = doe_if->cb + i;
		cb->size = cmd_buffer_size;
		cb->base = dmam_alloc_coherent(dev, cmd_buffer_size,
					       &cb->dma_base, GFP_KERNEL);
		if (!cb->base)
			return ERR_PTR(-ENOMEM);
	}

	/* alloc event buffer */
	eq = &doe_if->eq;
	eq->depth = eq_depth;
	eq->entry_size = eq_entry_size;
	eq->entry_bit = yusdoe_get_order(eq_entry_size);
	eq->base = dmam_alloc_coherent(dev, eq->depth * eq->entry_size,
				       &eq->dma_base, GFP_KERNEL);
	if (!eq->base)
		return ERR_PTR(-ENOMEM);

	/* alloc event pointer */
	eq->hw_tail_ptr = dmam_alloc_coherent(dev, sizeof(uint64_t),
					      &eq->dma_hw_tail, GFP_KERNEL);
	if (!eq->hw_tail_ptr)
		return ERR_PTR(-ENOMEM);

	dev_dbg(dev, "Init %s-if with eq 0x%px(0x%016llx) eq_ptr 0x%px(0x%016llx)\n",
					name, eq->base, eq->dma_base,
					eq->hw_tail_ptr, eq->dma_hw_tail);

	return doe_if;
}

irqreturn_t yusdoe_event_irq_handler(int vec, void *data)
{
	struct yusdoe_interface *doe_if = data;
	int ret;

	ret = yusdoe_check_irq(doe_if);
	if (ret)
		return IRQ_NONE;

	dev_dbg(doe_if->yusdoe->dev, "%s IRQ Recived!\n", doe_if->name);
	schedule_work(&doe_if->work);

	return IRQ_HANDLED;
}

static int yusdoe_irq_init(struct yusdoe_device *yusdoe)
{
	struct device *dev = yusdoe->pdev;
	struct pci_dev *pdev = to_pci_dev(dev);
	int msixcnt, msicnt, r_irq, w_irq;
	int ret = 0;

	msixcnt = pci_msix_vec_count(pdev);
	if (msixcnt) {
		ret = pci_alloc_irq_vectors(pdev, msixcnt, msixcnt, PCI_IRQ_MSIX);
		if (ret != msixcnt) {
			dev_err(dev, "Failed enabling %d MSIx entries: %d\n",
					msixcnt, ret);
			return -ENOSPC;
		}

		dev_dbg(dev, "Enabled %d msix vectors\n", msixcnt);
		goto out;
	}

	msicnt = pci_msi_vec_count(pdev);
	if (msicnt) {
		ret = pci_alloc_irq_vectors(pdev, msicnt, msicnt, PCI_IRQ_MSI);
		if (ret != msicnt) {
			dev_err(dev, "Failed enabling %d MSI entries: %d\n",
					msicnt, ret);
			return -ENOSPC;
		}

		dev_dbg(dev, "Enabled %d msi vectors\n", msicnt);
		goto out;
	}

	dev_err(dev, "Not MSIX/MSI interrupt capable.\n");
	return -ENOSPC;

out:
	/* request write event irq */
	w_irq = pci_irq_vector(pdev, YUSODE_IRQ_WRITE_EQ);
	ret = request_threaded_irq(w_irq, yusdoe_event_irq_handler, NULL,
				   0, "yusdoe-weq", yusdoe->doe_write_if);
	if (ret < 0) {
		dev_err(dev, "Failed to allocate write interrupt.\n");
		goto err_with_weq;
	}

	/* request read event irq */
	r_irq = pci_irq_vector(pdev, YUSODE_IRQ_READ_EQ);
	ret = request_threaded_irq(r_irq, yusdoe_event_irq_handler, NULL,
				   0, "yusdoe-req", yusdoe->doe_read_if);
	if (ret < 0) {
		dev_err(dev, "Failed to allocate read interrupt.\n");
		goto err_with_req;
	}

	return 0;

err_with_req:
	free_irq(w_irq, yusdoe->doe_write_if);
err_with_weq:
	pci_free_irq_vectors(pdev);

	return ret;
}

static void yusdoe_irq_uninit(struct yusdoe_device *yusdoe)
{
	struct pci_dev *pdev = to_pci_dev(yusdoe->pdev);
	int w_irq = pci_irq_vector(pdev, YUSODE_IRQ_WRITE_EQ);
	int r_irq = pci_irq_vector(pdev, YUSODE_IRQ_READ_EQ);

	free_irq(r_irq, yusdoe->doe_read_if);
	free_irq(w_irq, yusdoe->doe_write_if);
	pci_free_irq_vectors(pdev);
}

static int yusdoe_fops_open(struct inode *inode, struct file *filep)
{
	int dev_id = iminor(inode);
	struct yusdoe_device *yusdoe;

	list_for_each_entry(yusdoe, &yusdoe_list, list) {
		if (MINOR(yusdoe->devt) == dev_id) {
			filep->private_data = yusdoe;
			return 0;
		}
	}

	return -EINVAL;
}

static long yusdoe_fops_ioctl(struct file *filep, unsigned int cmd,
						  unsigned long arg)
{
	struct yusdoe_sw_cmd *sw_cmd;
	struct yusdoe_device *yusdoe = filep->private_data;
	int ret = 0;

	switch (cmd) {
	case YUSDOE_SEND_CMD:
		sw_cmd = yusdoe_sw_cmd_prepare(yusdoe, arg);
		if (IS_ERR(sw_cmd))
			return PTR_ERR(sw_cmd);

		ret = yusdoe_user_cmd_context(yusdoe, sw_cmd);
		if (!ret || sw_cmd->succeed)
			ret = copy_to_user((void *)arg, sw_cmd, sizeof(*sw_cmd));

		yusdoe_sw_cmd_unprepare(yusdoe, sw_cmd);

		return ret;
	default:
		return -EINVAL;
	}
}

static const struct file_operations yusdoe_fops = {
	.owner		= THIS_MODULE,
	.open		= yusdoe_fops_open,
	.unlocked_ioctl	= yusdoe_fops_ioctl,
};

static int yusdoe_cdev_create(struct yusdoe_device *yusdoe)
{
	int ret, dev_id;

	/* TODO: support multi doe */
	dev_id = 0;

	yusdoe->devt = MKDEV(MAJOR(yusdoe_devt), dev_id);
	cdev_init(&yusdoe->cdev, &yusdoe_fops);
	yusdoe->cdev.owner = THIS_MODULE;

	yusdoe->dev = device_create(yusdoe_class, yusdoe->pdev, yusdoe->devt,
						  yusdoe, "yusdoe-%d", dev_id);
	if (IS_ERR(yusdoe->dev)) {
		ret = PTR_ERR(yusdoe->dev);
		return ret;
	}

	cdev_set_parent(&yusdoe->cdev, &yusdoe->dev->kobj);
	ret = cdev_add(&yusdoe->cdev, yusdoe->devt, 1);
	if (ret)
		goto err_with_cdev_add;

	return 0;

err_with_cdev_add:
	device_destroy(yusdoe_class, yusdoe->devt);

	return ret;
}

static void yusdoe_cdev_destroy(struct yusdoe_device *yusdoe)
{
	device_destroy(yusdoe_class, yusdoe->devt);
	cdev_del(&yusdoe->cdev);
}

static int yusdoe_hw_resources_init(struct yusdoe_device *yusdoe)
{
	int ret, i, j;
	struct device *dev = yusdoe->pdev;
	struct yusdoe_table_param *param;
	struct yusdoe_table_spec *spec;

	init_waitqueue_head(&yusdoe->wait);
	mutex_init(&yusdoe->mutex);
	yusdoe->tbl_bitmap = devm_kzalloc(dev, ((YUSDOE_USER_TBL_NUM - 1) /
					sizeof(unsigned long) + 1) *
					sizeof(unsigned long), GFP_KERNEL);
	if (!yusdoe->tbl_bitmap)
		return -ENOMEM;

	/* init param of special table in sram */
	param = &yusdoe->param[YUSDOE_MIU_PARAM_TABLE];
	param->index_mask = YUSDOE_USER_TBL_NUM + 1;
	param->dov_len = sizeof(struct yusdoe_miu_param);

	param = &yusdoe->param[YUSDOE_HIE_PARAM_TABLE];
	param->index_mask = YUSDOE_USER_TBL_NUM - 1;
	param->dov_len = sizeof(struct yusdoe_hie_param);

	param = &yusdoe->param[YUSDOE_AIE_PARAM_TABLE];
	param->index_mask = YUSDOE_USER_TBL_NUM + 1;
	param->dov_len = sizeof(struct yusdoe_aie_param);

	param = &yusdoe->param[YUSDOE_CACHE_CONFIG_TABLE];
	param->index_mask = YUSDOE_USER_TBL_NUM - 1;
	param->dov_len = sizeof(struct yusdoe_cache_config);

	param = &yusdoe->param[YUSDOE_INDEX_MANAGE_TABLE];
	param->index_mask = YUSDOE_USER_TBL_NUM - 1;
	param->dov_len = sizeof(struct yusdoe_index_param);

	param = &yusdoe->param[YUSDOE_HCU_PARAM_TABLE];
	param->index_mask = YUSDOE_HCU_PARAM_CNT;
	param->dov_len = sizeof(struct yusdoe_hcu_param);

	/* init global hcu parameter */
	ret = yusdoe_hcu_init(yusdoe);
	if (ret)
		return ret;

	/* init param of special table in ddr */
	param = &yusdoe->param[YUSDOE_INDEX_RES_TABLE];
	param->index_mask = YUSDOE_DDR1_SIZE / YUSDOE_INDEX_ITEM_SIZE - 1;
	param->dov_len = YUSDOE_INDEX_ITEM_SIZE;
	spec = &yusdoe->spec[YUSDOE_INDEX_RES_TABLE];
	spec->miu_param.item_len = cpu_to_le16(YUSDOE_INDEX_ITEM_SIZE);
	spec->miu_param.item_size = yusdoe_get_order(param->dov_len);
	spec->miu_param.ddr_channel = 1;
	spec->aie_param.item_size = spec->miu_param.item_size;
	spec->aie_param.data_len = spec->miu_param.item_len;
	spec->aie_param.index_mask = cpu_to_le32(param->index_mask);
	spec->aie_param.valid = 1;
	/* index-res table disable cache_status and use array_write command */
	spec->aie_param.cache_status_dis = 1;

	/*
	 * submit air/miu_param for table 239 (hash index resource) which
	 * covers the entire array_ddr1 address space.
	 * index resource and table 238 (hash flush table) will be initialized
	 * at the stage of HashTBL Create.
	 */
	ret = yusdoe_submit_ddr_spec(yusdoe, YUSDOE_INDEX_RES_TABLE, NULL, NULL);
	if (ret)
		return ret;

	/* init memory resources */
	yusdoe->ddr0_hash = yusur_mm_init(0, YUSDOE_DDR0_SIZE,
					  YUSDOE_DDR_ALIGN, "ddr0_hash");
	if (IS_ERR(yusdoe->ddr0_hash))
		return PTR_ERR(yusdoe->ddr0_hash);

	yusdoe->ddr1_array = yusur_mm_init(0, YUSDOE_DDR1_SIZE,
					   YUSDOE_DDR_ALIGN, "ddr1_array");
	if (IS_ERR(yusdoe->ddr1_array)) {
		ret = PTR_ERR(yusdoe->ddr1_array);
		goto err_with_ddr1;
	}

	for (i = 0; i < YUSDOE_L1CACHE_NUM; ++i) {
		yusdoe->l1_cache[i] = yusur_mm_init(0, YUSDOE_L1CACHE_SIZE,
					 YUSDOE_CACHE_ALIGN, "l1_cache");
		if (IS_ERR(yusdoe->l1_cache)) {
			ret = PTR_ERR(yusdoe->l1_cache);
			goto err_with_l1cache;
		}

		yusdoe->l1_tag[i] = yusur_mm_init(0, YUSDOE_L1CACHE_TAG_NUM,
					 YUSDOE_CACHE_TAG_ALIGN, "l1_tag");
		if (IS_ERR(yusdoe->l1_tag[i])) {
			ret = PTR_ERR(yusdoe->l1_tag[i]);
			goto err_with_l1_tag;
		}
	}

	yusdoe->l2_cache = yusur_mm_init(0, YUSDOE_L2CACHE_SIZE,
					 YUSDOE_CACHE_ALIGN, "l2_cache");
	if (IS_ERR(yusdoe->l2_cache)) {
		ret = PTR_ERR(yusdoe->l2_cache);
		goto err_with_l2cache;
	}

	yusdoe->l2_tag = yusur_mm_init(0, YUSDOE_L2CACHE_TAG_NUM,
					 YUSDOE_CACHE_TAG_ALIGN, "l2_tag");
	if (IS_ERR(yusdoe->l2_tag)) {
		ret = PTR_ERR(yusdoe->l2_tag);
		goto err_with_l2_tag;
	}

	yusdoe->index_sram = yusur_mm_init(0, YUSDOE_INDEX_SRAM_SIZE,
					   YUSDOE_INDEX_SRAM_ALIGN, "index_sram");
	if (IS_ERR(yusdoe->index_sram)) {
		ret = PTR_ERR(yusdoe->index_sram);
		goto err_with_index_sram;
	}

	return 0;

err_with_index_sram:
	yusur_mm_uninit(yusdoe->l2_tag);
err_with_l2_tag:
	yusur_mm_uninit(yusdoe->l2_cache);
err_with_l1_tag:
	yusur_mm_uninit(yusdoe->l1_cache[i]);
err_with_l2cache:
err_with_l1cache:
	for (j = 0; j < i; ++j) {
		yusur_mm_uninit(yusdoe->l1_cache[j]);
		yusur_mm_uninit(yusdoe->l1_tag[j]);
	}
	yusur_mm_uninit(yusdoe->ddr1_array);
err_with_ddr1:
	yusur_mm_uninit(yusdoe->ddr0_hash);

	return ret;
}

static void yusdoe_hw_resources_uninit(struct yusdoe_device *yusdoe)
{
	int i;

	yusur_mm_uninit(yusdoe->index_sram);
	yusur_mm_uninit(yusdoe->l2_cache);

	for (i = 0; i < YUSDOE_L1CACHE_NUM; ++i)
		yusur_mm_uninit(yusdoe->l1_cache[i]);

	yusur_mm_uninit(yusdoe->ddr1_array);
	yusur_mm_uninit(yusdoe->ddr0_hash);
}

static int yusdoe_probe(struct pci_dev *pdev, const struct pci_device_id *id)
{
	int ret;
	struct device *dev = &pdev->dev;
	struct yusdoe_device *yusdoe;

	yusdoe = devm_kzalloc(dev, sizeof(*yusdoe), GFP_KERNEL);
	if (!yusdoe)
		return -ENOMEM;

	yusdoe->pdev = dev;
	pci_set_drvdata(pdev, yusdoe);

	/* Interface buffer init, include cmd buffer and event queue buffer */
	yusdoe->doe_write_if =
		yusdoe_alloc_interface(yusdoe, "write",
					       YUSDOE_WRITE_CMD_BUFFER_SIZE,
					       YUSDOE_WRITE_CMD_BUFFER_CNT,
					       YUSODE_IRQ_WRITE_EQ,
					       YUSDOE_WRITE_EVENTQ_ENTRY_SIZE,
					       YUSDOE_WRITE_EVENTQ_DEPTH);
	if (IS_ERR(yusdoe->doe_write_if))
		return PTR_ERR(yusdoe->doe_write_if);

	yusdoe->doe_read_if =
		yusdoe_alloc_interface(yusdoe, "read",
					       YUSDOE_READ_CMD_BUFFER_SIZE,
					       YUSDOE_READ_CMD_BUFFER_CNT,
					       YUSODE_IRQ_READ_EQ,
					       YUSDOE_READ_EVENTQ_ENTRY_SIZE,
					       YUSDOE_READ_EVENTQ_DEPTH);
	if (IS_ERR(yusdoe->doe_read_if))
		return PTR_ERR(yusdoe->doe_read_if);

	yusdoe->doe_read_if->is_read = 1;

	/* IO resource init */
	ret = pci_enable_device_mem(pdev);
	if (ret < 0) {
		dev_err(dev, "Failed to enable device mem!\n");
		return ret;
	}

	yusdoe->doe_base = pci_iomap(pdev, YUSDOE_REG_BAR, 0);
	if (!yusdoe->doe_base) {
		ret = -EIO;
		goto err_with_doe_iomap;
	}

	/* DOE hardware resources init */
	ret = yusdoe_hw_resources_init(yusdoe);
	if (ret)
		goto err_with_hw_res;

	/* DOE interrupt init */
	ret = yusdoe_irq_init(yusdoe);
	if (ret)
		goto err_with_irq;

	/* Create the device node */
	ret = yusdoe_cdev_create(yusdoe);
	if (ret)
		goto err_with_cdev;

	/* Setup PCIe */
	ret = dma_set_mask_and_coherent(dev, DMA_BIT_MASK(64));
	if (ret < 0)
		goto err_with_setmask;

	pci_set_master(pdev);

	list_add(&yusdoe->list, &yusdoe_list);

	/* init doe event and dma config */
	ret = yusdoe_reg_init(yusdoe);
	if (ret) {
		dev_err(dev, "DOE register init fail %d!\n", ret);
		goto err_with_reg_init;
	}

	dev_dbg(dev, "Install DOE successfully, version %08x\n",
			readl(yusdoe->doe_base + YUSDOE_VERSION));

	/* schedule poll work to send hash index resource special table */
	schedule_work(&yusdoe->doe_write_if->work);

	return 0;

err_with_reg_init:
err_with_setmask:
	yusdoe_cdev_destroy(yusdoe);
err_with_cdev:
	yusdoe_irq_uninit(yusdoe);
err_with_irq:
	yusdoe_hw_resources_uninit(yusdoe);
err_with_hw_res:
	pci_iounmap(pdev, yusdoe->doe_base);
err_with_doe_iomap:
	pci_disable_device(pdev);
	return ret;
}

static void yusdoe_remove(struct pci_dev *pdev)
{
	struct yusdoe_device *yusdoe = pci_get_drvdata(pdev);

	dev_dbg(yusdoe->pdev, "DOE %d Removed\n", 0);
	list_del(&yusdoe->list);
	pci_clear_master(pdev);
	yusdoe_cdev_destroy(yusdoe);
	yusdoe_irq_uninit(yusdoe);
	yusdoe_hw_resources_uninit(yusdoe);
	pci_iounmap(pdev, yusdoe->doe_base);
	pci_disable_device(pdev);
}

static struct pci_driver yusdoe_driver = {
	.name = "yusdoe",
	.id_table = yusdoe_dev_ids,
	.probe = yusdoe_probe,
	.remove = yusdoe_remove,
};

static int __init yusdoe_init(void)
{
	int ret;

	INIT_LIST_HEAD(&yusdoe_list);

	yusdoe_class = class_create(THIS_MODULE, "yusdoe");
	if (IS_ERR(yusdoe_class))
		return PTR_ERR(yusdoe_class);

	ret = alloc_chrdev_region(&yusdoe_devt, 0, MINORMASK, "yusdoe");
	if (ret)
		goto err_with_devt;

	ret = pci_register_driver(&yusdoe_driver);
	if (ret)
		goto err_with_driver;

	pr_info("yusdoe init with major number:%d\n", MAJOR(yusdoe_devt));

	return 0;

err_with_driver:
	unregister_chrdev_region(yusdoe_devt, MINORMASK);
err_with_devt:
	class_destroy(yusdoe_class);

	return ret;
}
module_init(yusdoe_init);

static void __exit yusdoe_exit(void)
{
	pci_unregister_driver(&yusdoe_driver);
	unregister_chrdev_region(yusdoe_devt, MINORMASK);
	class_destroy(yusdoe_class);
}
module_exit(yusdoe_exit);

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Jianing Ren <renjn@yusur.com>");
MODULE_DESCRIPTION("Driver for Data Offload Engine.");
