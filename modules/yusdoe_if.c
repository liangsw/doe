#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/io.h>
#include <linux/iopoll.h>
#include <linux/llist.h>

#include "yusdoe.h"

#define PLDA_ISTATUS_HOST		0x18c

int yusdoe_reg_init(struct yusdoe_device *yusdoe)
{
	struct yusdoe_interface *doe_if[2] = {
		yusdoe->doe_write_if,
		yusdoe->doe_read_if,
	};
	uint32_t offset[2] = {
		YUSDOE_WR_CHANNEL_BASE,
		YUSDOE_RD_CHANNEL_BASE,
	};
	struct yusdoe_event_queue *eq;
	uint32_t val;
	void __iomem *dma_reg_base;
	int i, ret;

	for (i = 0; i < 2; i++) {
		dma_reg_base = yusdoe->doe_base + offset[i];
		doe_if[i]->dma_reg_base = dma_reg_base;
		eq = &doe_if[i]->eq;

		yusdoe_writel(yusdoe, eq->entry_size,
				      dma_reg_base + YUSDOE_EVENT_SIZE);
		yusdoe_writel(yusdoe, eq->depth * eq->entry_size,
				      dma_reg_base + YUSDOE_EVENT_TOTAL_SIZE);
		yusdoe_writel(yusdoe, (uint32_t)eq->dma_base,
				      dma_reg_base + YUSDOE_EVENT_BASE_LOW);
		yusdoe_writel(yusdoe, (uint32_t)(eq->dma_base >> 32),
				      dma_reg_base + YUSDOE_EVENT_BASE_HIGH);
		yusdoe_writel(yusdoe, (uint32_t)eq->dma_hw_tail,
				      dma_reg_base + YUSDOE_EVENT_PTR_LOW);
		yusdoe_writel(yusdoe, (uint32_t)(eq->dma_hw_tail >> 32),
				      dma_reg_base + YUSDOE_EVENT_PTR_HIGH);
	}

	/* DOE reset must be asserted after event initial */
	yusdoe_writel(yusdoe, 1, yusdoe->doe_base + YUSDOE_RESET);
	ret = readl_poll_timeout(yusdoe->doe_base + YUSDOE_RESET, val,
				 !val, 100, 3000);
	if (ret)
		return ret;

	return 0;
}

static int _yusdoe_send_cmd(struct yusdoe_interface *doe_if,
			    struct yusdoe_cmd_buffer *cb)
{
	struct yusdoe_device *yusdoe = doe_if->yusdoe;
	uint32_t len = cb->ptr;

	/* Let the cmd buffer alignment */
	len = (len + YUSDOE_CMDDMA_ALIGN) & ~YUSDOE_CMDDMA_ALIGN;

	/* If there is the reserved 32 Bytes data, set bit16(CMD_VALID) to 0 */
	if (cb->ptr & YUSDOE_CMDDMA_ALIGN)
		memset(cb->base + cb->ptr, 0, 32);

	/* Write dma reg to trigger send */
	yusdoe_writel(yusdoe, (uint32_t)cb->dma_base,
			      doe_if->dma_reg_base + YUSDOE_CMD_ADDR_LOW);
	yusdoe_writel(yusdoe, (uint32_t)(cb->dma_base >> 32),
			      doe_if->dma_reg_base + YUSDOE_CMD_ADDR_HIGH);
	yusdoe_writel(yusdoe, len, doe_if->dma_reg_base + YUSDOE_CMD_LEN);
	yusdoe_writel(yusdoe, 1, doe_if->dma_reg_base + YUSDOE_CMD_CONTROL);

	dev_dbg(yusdoe->dev, "Send %s cmd buffer %px.%d\n", doe_if->name,
						cb->base, cb->ptr);
	buffer_dump(yusdoe, "cmd buffer", cb->base, len);
	cb->ptr = 0;
	cb->cmd_cnt = 0;

	return 0;
}

int yusdoe_dma_busy(struct yusdoe_interface *doe_if)
{
	return readl(doe_if->dma_reg_base + YUSDOE_CMD_CONTROL) & 0x1;
}

/* cmd buffer consumer */
int yusdoe_send_cmd(struct yusdoe_interface *doe_if)
{
	struct yusdoe_device *yusdoe = doe_if->yusdoe;
	struct yusdoe_cmd_buffer *cb = NULL;

	if (yusdoe_dma_busy(doe_if))
		return -EBUSY;

	/* Send head cb, add head pointer */
	if (doe_if->cb_head != doe_if->cb_tail) {
		dev_dbg(yusdoe->dev, "Send cb head %d/%d\n", doe_if->cb_head,
							     doe_if->cb_tail);
		cb = &doe_if->cb[doe_if->cb_head];
		CB_MOVE_HEAD(doe_if);
	/* Send tail cb, add both head and tail */
	} else if (doe_if->cb[doe_if->cb_tail].ptr) {
		dev_dbg(yusdoe->dev, "Send cb tail %d\n", doe_if->cb_tail);
		cb = &doe_if->cb[doe_if->cb_tail];
		CB_MOVE_HEAD(doe_if);
		CB_MOVE_TAIL(doe_if);
	}

	/* Real DMA operations */
	if (cb)
		_yusdoe_send_cmd(doe_if, cb);

	return 0;
}

#ifdef PLDA_VERSION
int yusdoe_check_irq(struct yusdoe_interface *doe_if)
{
	uint32_t val;

	/* Check interrupt status register */
	val = readl(doe_if->yusdoe->doe_base + PLDA_ISTATUS_HOST);
	if (!(val & BIT(doe_if->msi_index)))
		return -EINVAL;

	return 0;
}
void yusdoe_clean_irq(struct yusdoe_interface *doe_if)
{
	struct yusdoe_device *yusdoe = doe_if->yusdoe;
	uint32_t val;

	/* Write 1 to clean interrupt status, enable interrupt */
	val = readl(doe_if->yusdoe->dma_base + PLDA_ISTATUS_HOST);
	if (val & BIT(doe_if->msi_index))
		writel(BIT(doe_if->msi_index), yusdoe->dma_base + PLDA_ISTATUS_HOST);
}
#else
int yusdoe_check_irq(struct yusdoe_interface *doe_if)
{
	return 0;
}

void yusdoe_clean_irq(struct yusdoe_interface *doe_if) {}
#endif
