#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/io.h>
#include <linux/llist.h>
#include <linux/mm.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/wait.h>
#include <linux/workqueue.h>
#include <linux/uaccess.h>

#include "yusdoe.h"

static const uint32_t yusdoe_default_hcu[YUSDOE_HCU_PARAM_CNT] = {
	0x00010203,		/* param0: Byte 0-7 */
	0x04050607,		/* param1: Byte 8-15 */
	0x08090a0b,		/* param2: Byte 16-23 */
	0x0c0d0e0f,		/* param3: Byte 24-31 */
	0x00112233,		/* param4: Byte 32-39 */
	0x44556677,		/* param5: Byte 40-47 */
	0x8899aabb,		/* param6: Byte 48-55 */
	0xccddeeff,		/* param7: Byte 56-63 */
	0xffeeddcc,		/* param8: Byte 64-71 */
	0xbbaa9988,		/* param9: Byte 72-79 */
	0x77665544,		/* param10: Byte 80-87 */
	0x33221100,		/* param11: Byte 88-95 */
	0x0f0e0d0c,		/* param12: Byte 96-103 */
	0x0b0a0908,		/* param13: Byte 104-111 */
	0x07060504,		/* param14: Byte 112-119 */
	0x03020100,		/* param15: Byte 120-127 */
};

static int check_table_param(struct yusdoe_device *yusdoe, uint8_t table_id,
			     struct yusdoe_table_param *param)
{
	int i, temp, tag_num;

	/* check table id */
	if (table_id > YUSDOE_USER_TBL_NUM) {
		dev_err(yusdoe->dev, "Table ID %d exceeds the maximum value!\n", table_id);
		return -EINVAL;
	}

	/* check cache config */
	if (!param->use_cache)
		return 0;

	temp = 0;
	/* l1 cache must be configed */
	for (i = 0; i < YUSDOE_L1CACHE_NUM; ++i) {
		tag_num = param->l1_cache_ways[i];
		if (tag_num > YUSDOE_L1CACHE_TAG_NUM) {
			dev_err(yusdoe->dev,
				"L1 cache ways can't more than 32\n");
			return -EINVAL;
		}

		temp |= tag_num;
	}

	if (temp == 0) {
		dev_err(yusdoe->dev, "L1 cache ways can't be all zero.\n");
		return -EINVAL;
	}

	if (param->shared_tbl &&
			(param->l2_cache_ways == 0 || param->l2_cache_ways > 128)) {
		dev_err(yusdoe->dev, "L2 cache ways is in range of (0,128]. %d\n",
							param->l2_cache_ways);
		return -EINVAL;
	}

	return 0;
}

static int yusdoe_sw_cmd_valid(struct yusdoe_device *yusdoe,
			       struct yusdoe_sw_cmd *sw_cmd)
{
	uint8_t tbl_id = sw_cmd->tbl_id;
	bool is_hash;

	is_hash = false;

	switch (sw_cmd->opcode) {
		case YUSDOE_SW_RAW_CMD:
			return 0;

		/* create table */
		case YUSDOE_SW_CREATE_ARRAY:
		case YUSDOE_SW_CREATE_HASH:
			return check_table_param(yusdoe, tbl_id, &sw_cmd->tbl_param);

		/* delete table */
		case YUSDOE_SW_DELETE_ARRAY:
		case YUSDOE_SW_DELETE_HASH:
			if ((tbl_id >= YUSDOE_USER_TBL_NUM) ||
			    !test_and_clear_bit(tbl_id, yusdoe->tbl_bitmap))
				return -EINVAL;
			return 0;

		/* hash operation for table type check */
		case YUSDOE_SW_HASH_INSERT:
		case YUSDOE_SW_HASH_DELETE:
		case YUSDOE_SW_HASH_QUERY:
		case YUSDOE_SW_HASH_INSERT_BATCH:
		case YUSDOE_SW_HASH_DELETE_BATCH:
		case YUSDOE_SW_HASH_QUERY_BATCH:
			is_hash = true;
			break;

		case YUSDOE_SW_ARRAY_LOAD:
		case YUSDOE_SW_ARRAY_STORE:
		case YUSDOE_SW_ARRAY_READ_CLEAR:
		case YUSDOE_SW_ARRAY_WRITE:
		case YUSDOE_SW_ARRAY_READ:
			if (sw_cmd->index > yusdoe->param[tbl_id].index_mask)
				return -EINVAL;

			break;

		default:
			break;
	}

	if (tbl_id >= YUSDOE_USER_TBL_NUM ||
			!test_bit(tbl_id, yusdoe->tbl_bitmap) ||
			(is_hash != !!yusdoe->param[tbl_id].key_len)) {
		dev_err(yusdoe->dev, "Error tbl ID %d!\n", tbl_id);
		return -EINVAL;
	}

	return 0;
}

static void *yusdoe_address_map(unsigned long uaddr, uint32_t size,
				uint32_t *nr_pages_p, struct page ***pages_p)
{
	int32_t nr_pages, nr_pinned;
	struct page **pages;
	unsigned long offset;
	void *addr;
	int ret, i;

	/* get the page offset and number */
	offset = uaddr & (PAGE_SIZE - 1);
	nr_pages = (size + offset + PAGE_SIZE - 1) >> PAGE_SHIFT;

	pages = kcalloc(nr_pages, sizeof(struct page *), GFP_KERNEL);
	if (!pages)
		return ERR_PTR(-ENOMEM);

	/* get physical pages */
	nr_pinned = get_user_pages_fast(uaddr - offset, nr_pages, 0, pages);
	if (nr_pinned != nr_pages) {
		ret = -EFAULT;
		goto err_with_get_pages;
	}

	/* vmap to kernel */
	addr = vmap(pages, nr_pages, VM_MAP, PAGE_KERNEL);
	if (!addr) {
		ret = -ENOMEM;
		goto err_with_vmap;
	}
	addr += offset;

	*pages_p = pages;
	*nr_pages_p = nr_pages;

	return addr;

err_with_vmap:
err_with_get_pages:
	if (pages && (nr_pinned > 0)) {
		for (i = 0; i < nr_pinned; i++)
			put_page((pages)[i]);
	}
	kfree(pages);

	return ERR_PTR(ret);
}

static void yusdoe_address_unmap(void *addr, uint32_t nr_pages, struct page **pages)
{
	int i;
	unsigned long offset;

	offset = (uint64_t)addr & (PAGE_SIZE - 1);
	addr -= offset;

	vunmap(addr);
	for (i = 0; i < nr_pages; i++)
		put_page((pages)[i]);
	kfree(pages);
}

struct yusdoe_sw_cmd *yusdoe_sw_cmd_prepare(struct yusdoe_device *yusdoe,
					    unsigned long arg)
{
	struct yusdoe_sw_cmd *sw_cmd;
	const void *raw_cmd;
	uint32_t batch_koi_len = 0;
	uint32_t batch_pair_len = 0;
	uint8_t key_len, dov_len;
	int ret;

	sw_cmd = kzalloc(sizeof(*sw_cmd), GFP_KERNEL);
	if (!sw_cmd)
		return ERR_PTR(-ENOMEM);

	if (copy_from_user(sw_cmd, (const void *)arg, sizeof(*sw_cmd))) {
		ret = -EIO;
		goto err_with_sw_cmd;
	}

	ret = yusdoe_sw_cmd_valid(yusdoe, sw_cmd);
	if (ret)
		goto err_with_sw_cmd;

	key_len = yusdoe->param[sw_cmd->tbl_id].key_len;
	dov_len = yusdoe->param[sw_cmd->tbl_id].dov_len;

	/* Init number of pages to zero. set when batch operations */
	sw_cmd->koi_nr_pages = 0;
	sw_cmd->pair_nr_pages = 0;

	switch (sw_cmd->opcode) {
	/* user raw cmd */
	case YUSDOE_SW_RAW_CMD:
		raw_cmd = (const void *)(sw_cmd->cmd);
		sw_cmd->cmd = kzalloc(sw_cmd->cmd_size, GFP_KERNEL);
		if (!sw_cmd->cmd) {
			ret = -ENOMEM;
			goto err_with_sw_cmd;
		}

		if (copy_from_user(sw_cmd->cmd, raw_cmd, sw_cmd->cmd_size)) {
			ret = -EIO;
			goto err_with_raw_cmd;
		}
		dev_dbg(yusdoe->dev, "Recive raw cmd opc 0x%x tbl %d\n",
				     *(uint8_t *)sw_cmd->cmd,
				     *(uint8_t *)(sw_cmd->cmd + 1));
		return sw_cmd;

	/* batch operations */
	case YUSDOE_SW_ARRAY_STORE_BATCH:
	case YUSDOE_SW_HASH_INSERT_BATCH:
		batch_pair_len = sw_cmd->cnt *
				 ((key_len ? key_len : 4) + dov_len);
		break;

	case YUSDOE_SW_HASH_DELETE_BATCH:
		batch_koi_len = sw_cmd->cnt * (key_len ? key_len : 4);
		break;

	case YUSDOE_SW_ARRAY_READ_CLEAR_BATCH:
	case YUSDOE_SW_ARRAY_LOAD_BATCH:
	case YUSDOE_SW_HASH_QUERY_BATCH:
		batch_koi_len = sw_cmd->cnt * (key_len ? key_len : 4);
		batch_pair_len = sw_cmd->cnt *
				 ((key_len ? key_len : 4) + dov_len);
		break;

	default:
		break;
	}

	if (batch_koi_len) {
		sw_cmd->koi_list =
			yusdoe_address_map((unsigned long)sw_cmd->koi_list,
					   batch_koi_len,
					   &sw_cmd->koi_nr_pages,
					   (struct page ***)&sw_cmd->koi_pages);
		if (IS_ERR(sw_cmd->koi_list)) {
			ret = PTR_ERR(sw_cmd->koi_list);
			goto err_with_koi_map;
		}
	}

	if (batch_pair_len) {
		sw_cmd->pair_list =
			yusdoe_address_map((unsigned long)sw_cmd->pair_list,
					   batch_pair_len,
					   &sw_cmd->pair_nr_pages,
					   (struct page ***)&sw_cmd->pair_pages);
		if (IS_ERR(sw_cmd->pair_list)) {
			ret = PTR_ERR(sw_cmd->pair_list);
			goto err_with_pair_map;
		}
	}

	dev_dbg(yusdoe->dev, "Recive cmd opc 0x%x tbl %d\n",
			     sw_cmd->opcode, sw_cmd->tbl_id);

	return sw_cmd;

err_with_pair_map:
	if (sw_cmd->pair_nr_pages)
		yusdoe_address_unmap(sw_cmd->pair_list, sw_cmd->pair_nr_pages,
				     (struct page **)sw_cmd->pair_pages);
err_with_koi_map:
err_with_raw_cmd:
	if (sw_cmd->opcode == YUSDOE_SW_RAW_CMD)
		kfree(sw_cmd->cmd);
err_with_sw_cmd:
	kfree(sw_cmd);

	return ERR_PTR(ret);
}

void yusdoe_sw_cmd_unprepare(struct yusdoe_device *yusdoe,
			     struct yusdoe_sw_cmd *sw_cmd)
{
	if (sw_cmd->pair_nr_pages)
		yusdoe_address_unmap(sw_cmd->pair_list, sw_cmd->pair_nr_pages,
				     (struct page **)sw_cmd->pair_pages);
	if (sw_cmd->koi_nr_pages)
		yusdoe_address_unmap(sw_cmd->koi_list, sw_cmd->koi_nr_pages,
				     (struct page **)sw_cmd->koi_pages);
	if (sw_cmd->opcode == YUSDOE_SW_RAW_CMD)
		kfree(sw_cmd->cmd);
	kfree(sw_cmd);
}

static int yusdoe_alloc_cacheline(struct yusdoe_device *yusdoe,
				  struct yusur_mm *cache_mm,
				  struct yusur_mm *tag_mm,
				  struct yusdoe_cache_config *cache_config,
				  uint8_t tag_num)
{
	uint16_t size, addr;
	int64_t tag_base;
	int ret;

	if (tag_num == 0)
		return 0;

	/* alloc tag resource */
	tag_base = yusur_malloc(tag_mm, tag_num);
	if (tag_base == -ENOMEM) {
		dev_err(yusdoe->dev, "Alloc cache tag failed\n");
		return -ENOMEM;
	}

	/* alloc cache memory */
	size = ((YUSDOE_ITEM_NUM_PER_TAG * tag_num) <<
		 cache_config->data_size) / 32;
	size = roundup_pow_of_two(size);
	addr = yusur_malloc(cache_mm, size);
	if (addr == -ENOMEM) {
		dev_err(yusdoe->dev, "Alloc cache memory failed\n");
		ret = -ENOMEM;
		goto err_cache_memory;
	}

	cache_config->tag_base = tag_base;
	cache_config->data_base = addr;

	return 0;

err_cache_memory:
	yusur_free(tag_mm, tag_base);

	return ret;
}

static void yusdoe_free_cacheline(struct yusur_mm *cache_mm,
				  struct yusur_mm *tag_mm,
				  struct yusdoe_cache_config *cache_config)
{
	if (!cache_config->tag_num)
		return;

	yusur_free(cache_mm, cache_config->data_base);
	yusur_free(tag_mm, cache_config->tag_base);
	cache_config->valid = 0;
}

static int yusdoe_cache_alloc(struct yusdoe_device *yusdoe,
			      struct yusdoe_table_spec *spec_tbl,
			      struct yusdoe_table_param *param)
{
	int ret, i, j;
	uint32_t index_mask;
	uint8_t dov_size, tbl_type, tag_type, tag_num;
	uint16_t dov_len;
	struct yusur_mm *cache_mm, *tag_mm;
	struct yusdoe_cache_config *cache_config;

	tbl_type = param->shared_tbl ? 0 : 1;
	tag_type = param->key_len == 0 ? 0 : 1;
	dov_len = param->dov_len;
	dov_size = yusdoe_get_order(dov_len);
	dov_size = dov_size < 2 ? 2 : dov_size;
	if (param->key_len == 0) {
		tag_type = 0;
		index_mask = spec_tbl->aie_param.index_mask;
	} else {
		tag_type = 1;
		dov_len = ((param->key_len & 0xFF) << 8) | param->dov_len;
		index_mask = spec_tbl->hie_param.index_mask;
	}

	/* alloc l1 cache */
	for (i = 0; i < YUSDOE_L1CACHE_NUM; ++i) {
		cache_config = &spec_tbl->l1_cache[i];
		tag_num = param->l1_cache_ways[i];

		cache_config->tbl_type = tbl_type;
		cache_config->tag_num = tag_num;
		cache_config->tag_type = tag_type;
		cache_config->valid = 1;
		cache_config->data_len = dov_len;
		cache_config->data_size = dov_size;
		cache_config->op_index_mask = ~index_mask;

		cache_mm = yusdoe->l1_cache[i];
		tag_mm = yusdoe->l1_tag[i];

		ret = yusdoe_alloc_cacheline(yusdoe, cache_mm, tag_mm, cache_config, tag_num);
		if (ret != 0)
			goto err_alloc_l1_cache;
	}

	/* only shared-table need l2 cache */
	if (param->shared_tbl) {
		cache_config = &spec_tbl->l2_cache;
		tag_num = param->l2_cache_ways;

		cache_config->tbl_type = tbl_type;
		cache_config->tag_num = tag_num;
		cache_config->tag_type = tag_type;
		cache_config->valid = 1;
		cache_config->data_len = param->dov_len;
		cache_config->data_size = dov_size;
		cache_config->op_index_mask = ~index_mask;

		cache_mm = yusdoe->l2_cache;
		tag_mm = yusdoe->l2_tag;

		yusdoe_alloc_cacheline(yusdoe, cache_mm, tag_mm, cache_config, tag_num);
		if (ret != 0)
			goto err_alloc_l2_cache;
	}

	return 0;
err_alloc_l2_cache:
err_alloc_l1_cache:
	for (j = 0; j < i; j++) {
		cache_mm = yusdoe->l1_cache[j];
		tag_mm = yusdoe->l1_tag[j];
		cache_config = &spec_tbl->l1_cache[j];
		yusdoe_free_cacheline(cache_mm, tag_mm, cache_config);
		memset(cache_config, 0, sizeof(*cache_config));
	}

	return ret;
}

static void yusdoe_cache_free(struct yusdoe_device *yusdoe,
			      struct yusdoe_table_spec *spec_tbl,
			      struct yusdoe_table_param *param)
{
	int i;
	struct yusur_mm *cache_mm, *tag_mm;
	struct yusdoe_cache_config *cache_config;

	/* clear 16 l1 cache lines */
	for (i = 0; i < YUSDOE_L1CACHE_NUM; ++i) {
		cache_mm = yusdoe->l1_cache[i];
		tag_mm = yusdoe->l1_tag[i];
		cache_config = &spec_tbl->l1_cache[i];
		yusdoe_free_cacheline(cache_mm, tag_mm, cache_config);
	}

	/* clear l2 cache line */
	cache_mm = yusdoe->l2_cache;
	tag_mm = yusdoe->l2_tag;
	cache_config = &spec_tbl->l2_cache;
	yusdoe_free_cacheline(cache_mm, tag_mm, cache_config);
}

static int yusdoe_create_hashtbl(struct yusdoe_device *yusdoe,
				 uint8_t table_id,
				 struct yusdoe_table_param *param)
{
	struct yusdoe_table_spec *spec;
	struct yusdoe_table_spec *flush_spec =
					&yusdoe->spec[YUSDOE_HASH_FLUSH_TABLE];
	struct yusdoe_table_param *flush_param =
					&yusdoe->param[YUSDOE_HASH_FLUSH_TABLE];
	int ret;
	uint64_t size;
	int64_t addr;

	if (test_and_set_bit(table_id, yusdoe->tbl_bitmap)) {
		dev_err(yusdoe->dev, "Table ID %d is unavailable!\n", table_id);
		return -EINVAL;
	}

	spec = &yusdoe->spec[table_id];

	/* calculate item size */
	size = param->key_len + param->dov_len + 32;

	/* init hie_param special table */
	spec->miu_param.item_len = cpu_to_le16(size);
	spec->miu_param.item_size = yusdoe_get_order(size);
	spec->hie_param.item_size = spec->miu_param.item_size;
	spec->hie_param.key_len = cpu_to_le16(param->key_len);
	spec->hie_param.value_len = cpu_to_le16(param->dov_len);
	spec->hie_param.index_mask = cpu_to_le32(param->index_mask);
	spec->hie_param.cache_ways = param->cache_ways;
	spec->hie_param.ddr_channel = 0;
	spec->hie_param.valid = 1;

	/* calculate table size, include main and sub table */
	size = (uint64_t)(param->index_mask + 1) * (1 << spec->hie_param.item_size);
	size *= 2;
	addr = yusur_malloc(yusdoe->ddr0_hash, size);
	if (addr == -ENOMEM) {
		dev_info(yusdoe->dev, "Insufficient ddr0 for %llu Bytes!\n", size);
		ret = -ENOMEM;
		goto err_with_table;
	}

	/* init miu_param special table */
	spec->miu_param.ddr_base_high = (uint8_t)(addr >> 32);
	spec->miu_param.ddr_base_low = cpu_to_le32((uint32_t)addr);
	spec->miu_param.ddr_channel = 0;

	if (param->use_cache) {
		ret = yusdoe_cache_alloc(yusdoe, spec, param);
		if (ret)
			goto err_with_cache;
	}

	/* Resetting table parameters for each initialization */
	/* TODO: add lock to protect flush table */
	flush_param->index_mask = (param->index_mask << 1) + 1;
	flush_param->dov_len = param->key_len + param->dov_len + 32;
	flush_spec->miu_param.item_len = spec->miu_param.item_len;
	flush_spec->miu_param.item_size = spec->miu_param.item_size;
	flush_spec->miu_param.ddr_base_high = spec->miu_param.ddr_base_high;
	flush_spec->miu_param.ddr_base_low = spec->miu_param.ddr_base_low;
	flush_spec->miu_param.ddr_channel = 0;
	flush_spec->aie_param.item_size = spec->miu_param.item_size;
	flush_spec->aie_param.data_len = spec->miu_param.item_len;
	flush_spec->aie_param.valid = 1;
	flush_spec->aie_param.index_mask = cpu_to_le32(flush_param->index_mask);

	/* alloc index sram resources */
	size = param->index_sram_size;
	addr = yusur_malloc(yusdoe->index_sram, size);
	if (addr == -ENOMEM) {
		dev_info(yusdoe->dev, "Insufficient index sram for %llu Bytes!\n", size);
		ret = -ENOMEM;
		goto err_with_index_sram;
	}
	spec->index_param.ram_physic_base = cpu_to_le16((uint16_t)addr);

	/* alloc index ddr resources in array_ddr1 */
	size = (param->index_mask + 1) * 4;
	addr = yusur_malloc(yusdoe->ddr1_array, size);
	if (addr == -ENOMEM) {
		dev_info(yusdoe->dev, "Insufficient ddr1 for %llu Bytes!\n", size);
		ret = -ENOMEM;
		goto err_with_index_ddr;
	}

	/* Init index_param special table */
	spec->index_param.ddr_physic_base = cpu_to_le32((uint32_t)(addr >> 8));
	spec->index_param.ram_point = 0;
	spec->index_param.ddr_point = 0;
	spec->index_param.ddr_state = 0;

	memcpy(&yusdoe->param[table_id], param, sizeof(*param));

	dev_dbg(yusdoe->dev, "Create hash table %d: %d+%d(2^%d) * 0x%x\n",
						table_id, param->key_len,
						param->dov_len,
						spec->hie_param.item_size,
						param->index_mask + 1);
	return 0;

err_with_index_ddr:
	addr = le16_to_cpu(spec->index_param.ram_physic_base);
	yusur_free(yusdoe->index_sram, addr);
err_with_index_sram:
	if (param->use_cache)
		yusdoe_cache_free(yusdoe, spec, param);
err_with_cache:
	addr = (int64_t)spec->miu_param.ddr_base_high << 32;
	addr |= le32_to_cpu(spec->miu_param.ddr_base_low);
	yusur_free(yusdoe->ddr0_hash, addr);
err_with_table:
	clear_bit(table_id, yusdoe->tbl_bitmap);

	return ret;
}

static int yusdoe_delete_hashtbl(struct yusdoe_device *yusdoe,
				 uint8_t table_id,
				 struct yusdoe_table_param *param)
{
	struct yusdoe_table_spec *spec;
	int64_t addr;

	spec = &yusdoe->spec[table_id];

	spec->hie_param.valid = 0;

	/* free index ddr */
	addr = (int64_t)le32_to_cpu(spec->index_param.ddr_physic_base) << 8;
	yusur_free(yusdoe->ddr1_array, addr);

	/* free index sram */
	addr = le16_to_cpu(spec->index_param.ram_physic_base);
	yusur_free(yusdoe->index_sram, addr);

	/* free cache resource */
	yusdoe_cache_free(yusdoe, spec, param);

	/* free main and sub hash table */
	addr = (int64_t)spec->miu_param.ddr_base_high << 32;
	addr |= le32_to_cpu(spec->miu_param.ddr_base_low);
	yusur_free(yusdoe->ddr0_hash, addr);

	return 0;
}

static int yusdoe_create_arraytbl(struct yusdoe_device *yusdoe,
				  uint8_t table_id,
				  struct yusdoe_table_param *param)
{
	struct yusdoe_table_spec *spec;
	int ret;
	uint64_t size;
	int64_t addr;

	if (test_and_set_bit(table_id, yusdoe->tbl_bitmap)) {
		dev_err(yusdoe->dev, "Table ID %d is unavailable!\n", table_id);
		return -EINVAL;
	}

	spec = &yusdoe->spec[table_id];

	/* calculate item size */
	size = param->dov_len + 8;

	/* init aie_param special table */
	spec->miu_param.item_len = cpu_to_le16(size);
	spec->miu_param.item_size = yusdoe_get_order(size);
	spec->aie_param.item_size = spec->miu_param.item_size;
	// spec->aie_param.function = param->function;
	spec->aie_param.data_len = spec->miu_param.item_len;
	spec->aie_param.index_mask = cpu_to_le32(param->index_mask);
	spec->aie_param.cache_ways = param->cache_ways;
	spec->aie_param.ddr_channel = 1;
	spec->aie_param.valid = 1;

	/* calculate table size */
	size = (uint64_t)(param->index_mask + 1) * (1 << spec->aie_param.item_size);
	addr = yusur_malloc(yusdoe->ddr1_array, size);
	if (addr == -ENOMEM) {
		dev_info(yusdoe->dev, "Insufficient ddr1 for %llu Bytes!\n", size);
		ret = -ENOMEM;
		goto err_with_table;
	}
	dev_dbg(yusdoe->dev, "Alloc 0x%llx DDR1 at 0x%llx for array table %d\n",
						size, addr, table_id);

	/* init miu_param special table */
	spec->miu_param.ddr_base_high = (uint8_t)(addr >> 32);
	spec->miu_param.ddr_base_low = cpu_to_le32((uint32_t)addr);
	spec->miu_param.ddr_channel = 1;

	if (param->use_cache) {
		ret = yusdoe_cache_alloc(yusdoe, spec, param);
		if (ret)
			goto err_with_cache;
	}

	memcpy(&yusdoe->param[table_id], param, sizeof(*param));

	dev_dbg(yusdoe->dev, "Create array table %d: %d(2^%d) * 0x%x\n",
						table_id, param->dov_len,
						spec->aie_param.item_size,
						param->index_mask + 1);

	return 0;

err_with_cache:
	addr = (int64_t)spec->miu_param.ddr_base_high << 32;
	addr |= le32_to_cpu(spec->miu_param.ddr_base_low);
	yusur_free(yusdoe->ddr1_array, addr);
err_with_table:
	clear_bit(table_id, yusdoe->tbl_bitmap);

	return ret;
}

static int yusdoe_delete_arraytbl(struct yusdoe_device *yusdoe,
				  uint8_t table_id,
				  struct yusdoe_table_param *param)
{
	struct yusdoe_table_spec *spec;
	int64_t addr;

	spec = &yusdoe->spec[table_id];
	spec->aie_param.valid = 0;

	/* free cache resource */
	yusdoe_cache_free(yusdoe, spec, param);

	/* free main and sub hash table */
	addr = (int64_t)spec->miu_param.ddr_base_high << 32;
	addr |= le32_to_cpu(spec->miu_param.ddr_base_low);
	yusur_free(yusdoe->ddr1_array, addr);

	return 0;
}

static int yusdoe_complete_desc(struct yusdoe_desc *desc,
				struct yusdoe_event *event)
{
	struct yusdoe_device *yusdoe = desc->yusdoe;
	struct yusdoe_sw_cmd *parent = desc->parent;
	struct yusdoe_sw_cmd *cmd = desc->cmd;
	bool hash_read = false, array_read = false;
	uint16_t dov_len, key_len;
	void *base;

	/* TODO: add NULL pointer judgement */
	if (desc->cmd->opcode == YUSDOE_SW_ARRAY_LOAD ||
	    desc->cmd->opcode == YUSDOE_SW_ARRAY_READ ||
	    desc->cmd->opcode == YUSDOE_SW_ARRAY_READ_CLEAR ||
		desc->cmd->opcode == YUSDOE_SW_M3_READ)
		array_read = true;
	else if (desc->cmd->opcode == YUSDOE_SW_HASH_QUERY)
		hash_read = true;

	dov_len = yusdoe->param[cmd->tbl_id].dov_len;
	key_len = yusdoe->param[cmd->tbl_id].key_len;

	/* If the desc is one of the batch operation */
	if (parent) {
		if (event->status != YUSDOE_STATUS_SUCCESS) {
			parent->failed++;
			if (parent->err == 0) {
				parent->err = event->status;
			}
		} else {
			base = parent->pair_list;
			if (hash_read) {
				base += parent->succeed * (dov_len + key_len); 
				memcpy(base, cmd->key, key_len);
				memcpy(base + key_len, event + 1, dov_len);
			} else if (array_read) {
				base += parent->succeed * (dov_len + 4); 
				*(uint32_t *)base = cmd->index;
				memcpy(base + 4, event + 1, dov_len);
			}

			parent->succeed++;
		}

		/* All cmd excuated, wakeup user */
		if (parent->cnt == parent->succeed + parent->failed)
			wake_up_interruptible(&yusdoe->wait);
		
		kfree(cmd);
	} else {
		if (event->status != YUSDOE_STATUS_SUCCESS) {
			cmd->failed++;
			if (cmd->err == 0) {
				cmd->err = event->status;
			}
		} else {
			/* For user raw debug cmd, dov-len is uninitialized */
			if (!dov_len)
				dov_len = 128;

			if (hash_read) {
				memset(cmd->value, 0, sizeof(cmd->value));
				memcpy(cmd->value, event + 1, dov_len);
			} else if (array_read) {
				memset(cmd->data, 0, sizeof(cmd->data));
				memcpy(cmd->data, event + 1, dov_len);
			}

			cmd->succeed++;
		}

		wake_up_interruptible(&yusdoe->wait);
	}

	kfree(desc);

	return 0;
}

static int yusdoe_flush_table(struct yusdoe_device *yusdoe, uint32_t depth,
			      bool is_hash, struct yusdoe_sw_cmd *parent)
{
	struct yusdoe_sw_cmd *cmd;
	int ret;
	uint32_t i;

	for (i = 0; i < depth; i++) {
		cmd = kzalloc(sizeof(struct yusdoe_sw_cmd), GFP_KERNEL);
		if (!cmd)
			return -ENOMEM;

		cmd->opcode = YUSDOE_SW_ARRAY_WRITE;
		cmd->index = i;
		if (is_hash)
			cmd->tbl_id = YUSDOE_HASH_FLUSH_TABLE;
		else
			cmd->tbl_id = parent->tbl_id;

		ret = yusdoe_submit_cmd(yusdoe, cmd, parent,
					yusdoe_complete_desc);
		if (ret) {
			kfree(cmd);
			return ret;
		}
	}

	/*
	 * The cache command will send after flush table. Hardware maybe deal flush
	 * command befor the cache command finish, but this is not allow.
	 * The solution is add three hundreds commands at the end of flush table.
	 */
	for (i = 0; i < ADDITION_CMD_NUM; ++i) {
		cmd = kzalloc(sizeof(struct yusdoe_sw_cmd), GFP_KERNEL);
		if (!cmd)
			return -ENOMEM;

		cmd->opcode = YUSDOE_SW_ARRAY_WRITE;
		cmd->index = depth - 1;
		if (is_hash)
			cmd->tbl_id = YUSDOE_HASH_FLUSH_TABLE;
		else
			cmd->tbl_id = parent->tbl_id;

		ret = yusdoe_submit_cmd(yusdoe, cmd, parent,
					yusdoe_complete_desc);
		if (ret) {
			kfree(cmd);
			return ret;
		}
	}

	return 0;
}
static int yusdoe_init_hash_index(struct yusdoe_device *yusdoe, uint32_t depth,
				  struct yusdoe_sw_cmd *parent)
{
	struct yusdoe_sw_cmd *cmd;
	int ret;
	uint32_t i, j, index;
	uint32_t stride = YUSDOE_INDEX_ITEM_SIZE / 4;
	struct yusdoe_table_spec *spec = &yusdoe->spec[parent->tbl_id];
	uint32_t hash_index_base;

	hash_index_base = spec->index_param.ddr_physic_base;
	index = 0;

	for (i = 0; i < depth; i += stride) {
		cmd = kzalloc(sizeof(struct yusdoe_sw_cmd), GFP_KERNEL);
		if (!cmd)
			return -ENOMEM;

		cmd->opcode = YUSDOE_SW_ARRAY_WRITE;
		cmd->tbl_id = YUSDOE_INDEX_RES_TABLE;
		cmd->index = hash_index_base + index;

		/* fill array data as index of sub-table */
		for (j = 0; j < stride; j++)
			*((uint32_t *)cmd->data + j) = depth + index * stride + j;

		index++;

		ret = yusdoe_submit_cmd(yusdoe, cmd, parent,
					yusdoe_complete_desc);
		if (ret) {
			kfree(cmd);
			return ret;
		}
	}

	return 0;
}

static int yusdoe_submit_cache_spec(struct yusdoe_device *yusdoe, uint8_t spec_tbl,
			      struct yusdoe_sw_cmd *parent)
{
	struct yusdoe_sw_cmd *cmd;
	struct yusdoe_table_spec *spec = &yusdoe->spec[parent->tbl_id];
	int ret, i;

	for (i = 0; i < YUSDOE_L1CACHE_NUM; ++i) {
		if (spec->l1_cache[i].tag_num == 0)
			continue;

		/* submit l1 cache */
		cmd = kzalloc(sizeof(struct yusdoe_sw_cmd), GFP_KERNEL);
		if (!cmd)
			return -ENOMEM;

		cmd->opcode = YUSDOE_SW_ARRAY_WRITE;
		cmd->tbl_id = spec_tbl;
		cmd->index = parent->tbl_id + (i << 8) + (1 << 15);
		memcpy(cmd->data, &spec->l1_cache[i], sizeof(struct yusdoe_cache_config));

		ret = yusdoe_submit_cmd(yusdoe, cmd, parent, yusdoe_complete_desc);
		if (ret) {
			kfree(cmd);
			return ret;
		}

		parent->cnt++;
	}

	if (spec->l2_cache.tag_num == 0)
		return ret;

	/* submit l2 cache*/
	cmd = kzalloc(sizeof(struct yusdoe_sw_cmd), GFP_KERNEL);
	if (!cmd)
		return -ENOMEM;

	cmd->opcode = YUSDOE_SW_ARRAY_WRITE;
	cmd->tbl_id = spec_tbl;
	cmd->index = parent->tbl_id;
	memcpy(cmd->data, &spec->l2_cache, sizeof(struct yusdoe_cache_config));

	ret = yusdoe_submit_cmd(yusdoe, cmd, parent, yusdoe_complete_desc);
	if (ret) {
		kfree(cmd);
		return ret;
	}

	parent->cnt++;

	return 0;
}

int yusdoe_submit_ddr_spec(struct yusdoe_device *yusdoe, uint8_t spec_tbl,
			   struct yusdoe_sw_cmd *parent,
			   int (*complete_desc)(struct yusdoe_desc *,
						struct yusdoe_event *))
{
	struct yusdoe_sw_cmd *cmd;
	struct yusdoe_table_spec *spec = &yusdoe->spec[spec_tbl];
	int ret;

	/* submit miu table */
	cmd = kzalloc(sizeof(struct yusdoe_sw_cmd), GFP_KERNEL);
	if (!cmd)
		return -ENOMEM;

	cmd->opcode = YUSDOE_SW_ARRAY_WRITE;
	cmd->tbl_id = YUSDOE_MIU_PARAM_TABLE;
	cmd->index = spec_tbl;
	memcpy(cmd->data, &spec->miu_param, sizeof(struct yusdoe_miu_param));

	ret = yusdoe_submit_cmd(yusdoe, cmd, parent, complete_desc);
	if (ret) {
		kfree(cmd);
		return ret;
	}

	/* submit aie table */
	cmd = kzalloc(sizeof(struct yusdoe_sw_cmd), GFP_KERNEL);
	if (!cmd)
		return -ENOMEM;

	cmd->opcode = YUSDOE_SW_ARRAY_WRITE;
	cmd->tbl_id = YUSDOE_AIE_PARAM_TABLE;
	cmd->index = spec_tbl;
	memcpy(cmd->data, &spec->aie_param, sizeof(struct yusdoe_aie_param));

	ret = yusdoe_submit_cmd(yusdoe, cmd, parent, complete_desc);
	if (ret) {
		kfree(cmd);
		return ret;
	}

	return 0;
}

/*
 * Update spec table when creating user table
 *  @yusdoe: doe device
 *  @spec_tbl: the special table id to be updated
 *  @parent: software cmd to creat user table
 */
static int yusdoe_submit_spec(struct yusdoe_device *yusdoe, uint8_t spec_tbl,
			      struct yusdoe_sw_cmd *parent)
{
	struct yusdoe_sw_cmd *cmd;
	struct yusdoe_table_spec *spec = &yusdoe->spec[parent->tbl_id];
	void *data;
	uint32_t size;
	int ret;

	switch (spec_tbl) {
	case YUSDOE_INDEX_MANAGE_TABLE:
		data = &spec->index_param;
		size = sizeof(struct yusdoe_index_param);
		break;

	case YUSDOE_AIE_PARAM_TABLE:
		data = &spec->aie_param;
		size = sizeof(struct yusdoe_aie_param);
		break;

	case YUSDOE_HIE_PARAM_TABLE:
		data = &spec->hie_param;
		size = sizeof(struct yusdoe_hie_param);
		break;

	case YUSDOE_MIU_PARAM_TABLE:
		data = &spec->miu_param;
		size = sizeof(struct yusdoe_miu_param);
		break;
	
	case YUSDOE_HASH_FLUSH_TABLE:
		return yusdoe_submit_ddr_spec(yusdoe, YUSDOE_HASH_FLUSH_TABLE,
					      parent, yusdoe_complete_desc);

	default:
		return -EINVAL;
	}

	cmd = kzalloc(sizeof(struct yusdoe_sw_cmd), GFP_KERNEL);
	if (!cmd)
		return -ENOMEM;

	cmd->opcode = YUSDOE_SW_ARRAY_WRITE;
	cmd->tbl_id = spec_tbl;
	cmd->index = parent->tbl_id;
	memcpy(cmd->data, data, size);

	ret = yusdoe_submit_cmd(yusdoe, cmd, parent, yusdoe_complete_desc);
	if (ret) {
		kfree(cmd);
		return ret;
	}

	return 0;
}

int yusdoe_hcu_init(struct yusdoe_device *yusdoe)
{
	struct yusdoe_sw_cmd *cmd;
	int i, ret;

	for (i = 0; i < YUSDOE_HCU_PARAM_CNT; i++) {
		cmd = kzalloc(sizeof(struct yusdoe_sw_cmd), GFP_KERNEL);
		if (!cmd)
			return -ENOMEM;

		cmd->opcode = YUSDOE_SW_ARRAY_WRITE;
		cmd->tbl_id = YUSDOE_HCU_PARAM_TABLE;
		cmd->index = i;
		*(uint32_t *)cmd->data = cpu_to_le32(yusdoe_default_hcu[i]);

		ret = yusdoe_submit_cmd(yusdoe, cmd, NULL, NULL);
		if (ret) {
			kfree(cmd);
			return ret;
		}
	}

	return ret;
}

static int yusdoe_batch_ops(struct yusdoe_device *yusdoe,
			    struct yusdoe_sw_cmd *parent)
{
	uint8_t opcode;
	uint8_t tbl_id = parent->tbl_id;
	uint8_t key_len, dov_len;
	uint16_t stride = 0;
	void *input;
	struct yusdoe_sw_cmd *cmd;
	int i, ret;

	key_len = yusdoe->param[tbl_id].key_len;
	dov_len = yusdoe->param[tbl_id].dov_len;

	/* change opcode to sub cmd */
	switch (parent->opcode) {
	case YUSDOE_SW_ARRAY_LOAD_BATCH:
		opcode = YUSDOE_SW_ARRAY_LOAD;
		input = parent->koi_list;
		break;
	case YUSDOE_SW_ARRAY_STORE_BATCH:
		opcode = YUSDOE_SW_ARRAY_STORE;
		input = parent->pair_list;
		stride = dov_len;
		break;
	case YUSDOE_SW_ARRAY_READ_CLEAR_BATCH:
		opcode = YUSDOE_SW_ARRAY_READ_CLEAR;
		input = parent->koi_list;
		break;
	case YUSDOE_SW_HASH_INSERT_BATCH:
		opcode = YUSDOE_SW_HASH_INSERT;
		input = parent->pair_list;
		stride = dov_len;
		break;
	case YUSDOE_SW_HASH_DELETE_BATCH:
		opcode = YUSDOE_SW_HASH_DELETE;
		input = parent->koi_list;
		break;
	case YUSDOE_SW_HASH_QUERY_BATCH:
		opcode = YUSDOE_SW_HASH_QUERY;
		input = parent->koi_list;
		break;
	default:
		return -EINVAL;
	}

	stride += key_len ? key_len : 4;

	for (i = 0; i < parent->cnt; i++) {
		/* alloc sub cmd */
		cmd = kzalloc(sizeof(struct yusdoe_sw_cmd), GFP_KERNEL);
		if (!cmd)
			return -ENOMEM;

		cmd->opcode = opcode;
		cmd->tbl_id = parent->tbl_id;
		cmd->is_read = parent->is_read;

		if (!key_len)
			cmd->index = cpu_to_le32(*(uint32_t *)(input + i * stride));
		else
			memcpy(cmd->key, input + i * stride, key_len);

		if (opcode == YUSDOE_SW_ARRAY_STORE)
			memcpy(cmd->data, input + i * stride + 4, dov_len);

		if (opcode == YUSDOE_SW_HASH_INSERT)
			memcpy(cmd->value, input + i * stride + key_len, dov_len);


		ret = yusdoe_submit_cmd(yusdoe, cmd, parent, yusdoe_complete_desc);
		if (ret) {
			kfree(cmd);
			return ret;
		}
	}
	
	return 0;
}

int yusdoe_process_cmd(struct yusdoe_device *yusdoe, struct yusdoe_sw_cmd *cmd)
{
	int ret;
	uint32_t depth;

	switch (cmd->opcode) {
	case YUSDOE_SW_CREATE_ARRAY:
		mutex_lock(&yusdoe->mutex);
		ret = yusdoe_create_arraytbl(yusdoe, cmd->tbl_id,
						     &cmd->tbl_param);
		mutex_unlock(&yusdoe->mutex);
		if (ret)
			return ret;

		/* calculate total sub command for this operation */
		depth = yusdoe->param[cmd->tbl_id].index_mask + 1;

		/* submit spec table */
		cmd->cnt = 2;
		yusdoe_submit_spec(yusdoe, YUSDOE_MIU_PARAM_TABLE, cmd);
		yusdoe_submit_spec(yusdoe, YUSDOE_AIE_PARAM_TABLE, cmd);

		/* flush tbl */
		cmd->cnt += (depth + ADDITION_CMD_NUM);
		yusdoe_flush_table(yusdoe, depth, false, cmd);

		if (cmd->tbl_param.use_cache)
			ret = yusdoe_submit_cache_spec(yusdoe, YUSDOE_CACHE_CONFIG_TABLE, cmd);

		break;

	case YUSDOE_SW_DELETE_ARRAY:
		mutex_lock(&yusdoe->mutex);
		yusdoe_delete_arraytbl(yusdoe, cmd->tbl_id, &cmd->tbl_param);
		mutex_unlock(&yusdoe->mutex);

		cmd->cnt = 1;
		yusdoe_submit_spec(yusdoe, YUSDOE_AIE_PARAM_TABLE, cmd);
		yusdoe_submit_cache_spec(yusdoe, YUSDOE_CACHE_CONFIG_TABLE, cmd);

		/* clear data of cache config */
		memset(&yusdoe->spec[cmd->tbl_id].l1_cache, 0,
			sizeof(struct yusdoe_cache_config) * YUSDOE_L1CACHE_NUM);
		memset(&yusdoe->spec[cmd->tbl_id].l2_cache, 0,
			sizeof(struct yusdoe_cache_config));

		break;

	case YUSDOE_SW_CREATE_HASH:
		mutex_lock(&yusdoe->mutex);
		ret = yusdoe_create_hashtbl(yusdoe, cmd->tbl_id, &cmd->tbl_param);
		mutex_unlock(&yusdoe->mutex);
		if (ret)
			return ret;

		/* calculate total sub command for this operation */
		depth = yusdoe->param[cmd->tbl_id].index_mask + 1;

		/*
		 * calculate total cmd number of create hash table.
		 * submit MIU、HIE、CACHE_CONFIG、INDEX_MANAGE spec table use 1 cmd.
		 * submit HASH_FLUSH spce use 2 cmd.
		 * flush hash data use depth * 2 cmd.
		 * flush hash index use (depth -1) / 64 + 1 cmd.
		 */
		cmd->cnt = 5 + depth * 2 + (depth - 1) / 64 + 1 + ADDITION_CMD_NUM;

		/* submit spec table */
		yusdoe_submit_spec(yusdoe, YUSDOE_MIU_PARAM_TABLE, cmd);
		yusdoe_submit_spec(yusdoe, YUSDOE_HIE_PARAM_TABLE, cmd);
		yusdoe_submit_spec(yusdoe, YUSDOE_HASH_FLUSH_TABLE, cmd);

		/* flush index resources */
		yusdoe_init_hash_index(yusdoe, depth, cmd);

		/* flush tbl */
		ret = yusdoe_flush_table(yusdoe, depth * 2, true, cmd);

		/**
		 * Doe developer request to change the special table order.
		 * The index manage table must be submitted after index
		 * resource table. So the index resource can be read when
		 * the index manage table is submitted.
		 */
		yusdoe_submit_spec(yusdoe, YUSDOE_INDEX_MANAGE_TABLE, cmd);

		if (cmd->tbl_param.use_cache)
			yusdoe_submit_cache_spec(yusdoe, YUSDOE_CACHE_CONFIG_TABLE, cmd);

		break;

	case YUSDOE_SW_DELETE_HASH:
		mutex_lock(&yusdoe->mutex);
		yusdoe_delete_hashtbl(yusdoe, cmd->tbl_id, &cmd->tbl_param);
		mutex_unlock(&yusdoe->mutex);

		cmd->cnt = 1;

		yusdoe_submit_spec(yusdoe, YUSDOE_HIE_PARAM_TABLE, cmd);
		yusdoe_submit_cache_spec(yusdoe, YUSDOE_CACHE_CONFIG_TABLE, cmd);

		/* clear data of cache config */
		memset(&yusdoe->spec[cmd->tbl_id].l1_cache, 0,
			sizeof(struct yusdoe_cache_config) * YUSDOE_L1CACHE_NUM);
		memset(&yusdoe->spec[cmd->tbl_id].l2_cache, 0,
			sizeof(struct yusdoe_cache_config));

		break;

	/* alloc the desc for single cmd */
	case YUSDOE_SW_ARRAY_LOAD:
	case YUSDOE_SW_ARRAY_STORE:
	case YUSDOE_SW_ARRAY_READ_CLEAR:
	case YUSDOE_SW_HASH_INSERT:
	case YUSDOE_SW_HASH_DELETE:
	case YUSDOE_SW_HASH_QUERY:
	case YUSDOE_SW_RAW_CMD:
		ret = yusdoe_submit_cmd(yusdoe, cmd, NULL, yusdoe_complete_desc);
		if (ret)
			return ret;
		break;

	/* alloc the descs for batch cmd */
	case YUSDOE_SW_ARRAY_STORE_BATCH:
	case YUSDOE_SW_ARRAY_READ_CLEAR_BATCH:
	case YUSDOE_SW_ARRAY_LOAD_BATCH:
	case YUSDOE_SW_HASH_INSERT_BATCH:
	case YUSDOE_SW_HASH_DELETE_BATCH:
	case YUSDOE_SW_HASH_QUERY_BATCH:
		ret = yusdoe_batch_ops(yusdoe, cmd);
		if (ret)
			return ret;
		break;

	default:
		return -EINVAL;
	}

	return 0;
}

/* cmd buffer productor */
int yusdoe_enqueue_cmdbuffer(struct yusdoe_interface *doe_if,
			     struct yusdoe_desc *desc)
{
	struct yusdoe_device *yusdoe = doe_if->yusdoe;
	struct yusdoe_cmd_buffer *cb;
	struct yusdoe_sw_cmd *cmd = desc->cmd;
	struct yusdoe_hw_cmd_head *cmd_head;
	uint16_t key_len, dov_len, cmd_len;
	static bool prev_is_special = true;
	bool is_special;
	void *ptr;

	cb = doe_if->cb + doe_if->cb_tail;
	desc->cmd_tag = doe_if->cmd_tag;
	is_special = cmd->tbl_id >= YUSDOE_SPEC_TBL_BASE;
	/*
	 * There are three case to move the tail of cmd_buffer:
	 *  - The rest size may be not enough
	 *  - exceed the max cmd-cnt
	 *  - special table mixed with user table
	 */
	if (cb->ptr + YUSDOE_CMD_MAXSIZE > cb->size ||
	    cb->cmd_cnt >= YUSDOE_CB_MAX_CMD ||
	    (prev_is_special != is_special && cb->ptr)) {
		if (CB_IS_FULL(doe_if))
			return -ENOSPC;

		CB_MOVE_TAIL(doe_if);
		cb = doe_if->cb + doe_if->cb_tail;
	}
	prev_is_special = is_special;

	ptr = cb->base + cb->ptr;
	key_len = yusdoe->param[cmd->tbl_id].key_len;
	dov_len = yusdoe->param[cmd->tbl_id].dov_len;

	/* User table clear cmd. Add cache-pos */
	if (cmd->tbl_id < YUSDOE_USER_TBL_NUM &&
	    cmd->opcode == YUSDOE_SW_ARRAY_WRITE)
		dov_len += 8;

	/* length of opcode and table_id */
	cmd_len = 2;

	/* Init cmd head */
	cmd_head = (struct yusdoe_hw_cmd_head *)ptr;
	cmd_head->valid = 1;
	ptr += sizeof(*cmd_head);
	cmd_head->table_id = cmd->tbl_id;
	cmd_head->cmd_tag = cpu_to_le16(desc->cmd_tag);
	doe_if->cmd_id++;

	switch (cmd->opcode) {
	case YUSDOE_SW_RAW_CMD:
		ptr -= 2;
		memcpy(ptr, cmd->cmd, cmd->cmd_size);
		cmd_len = cmd->cmd_size;
		ptr += cmd->cmd_size;

		/* Transter the user-define opcode for desc-complete */
		cmd->opcode = *(uint8_t *)cmd->cmd;
		cmd->tbl_id = *(uint8_t *)(cmd->cmd + 1);
		break;

	case YUSDOE_SW_ARRAY_LOAD:
	case YUSDOE_SW_ARRAY_READ:
	case YUSDOE_SW_ARRAY_READ_CLEAR:
		cmd_head->opcode = cmd->opcode;
		*(uint32_t *)ptr = cpu_to_le32(cmd->index);
		ptr += 4;
		cmd_len += 4;
		break;

	case YUSDOE_SW_ARRAY_STORE:
	case YUSDOE_SW_ARRAY_WRITE:
		cmd_head->opcode = cmd->opcode;
		*(uint32_t *)ptr = cpu_to_le32(cmd->index);
		ptr += 4;
		cmd_len += 4;
		memcpy(ptr, cmd->data, dov_len);
		ptr += dov_len;
		cmd_len += dov_len;
		break;

	case YUSDOE_SW_HASH_QUERY:
		cmd_head->opcode = YUSDOE_HASH_QUERY;
		memcpy(ptr, cmd->key, key_len);
		ptr += key_len;
		cmd_len += key_len;
		break;

	case YUSDOE_SW_HASH_INSERT:
		cmd_head->opcode = YUSDOE_HASH_INSERT;
		memcpy(ptr, cmd->key, key_len);
		ptr += key_len;
		cmd_len += key_len;
		memcpy(ptr, cmd->value, dov_len);
		ptr += dov_len;
		cmd_len += dov_len;
		break;

	case YUSDOE_SW_HASH_DELETE:
		cmd_head->opcode = YUSDOE_HASH_DELETE;
		memcpy(ptr, cmd->key, key_len);
		ptr += key_len;
		cmd_len += key_len;
		break;

	default:
		break;
	}

	dev_dbg(yusdoe->dev, "Enqueue %s cmd%d 0x%04x (%d.%d-%ld) for OPC 0x%x TBL %d\n",
			     doe_if->name, cb->cmd_cnt, desc->cmd_tag,
			     doe_if->cb_tail, cb->ptr, ptr - cb->base,
			     cmd->opcode, cmd->tbl_id);

	cmd_head->cmd_len = cmd_len;
	cb->cmd_cnt++;
	cb->ptr = ptr - cb->base;
	/* Let the cmd buffer alignment */
	cb->ptr = (cb->ptr + YUSDOE_CMD_ALIGN) & ~YUSDOE_CMD_ALIGN;

	return 0;
}
