#ifndef __UAPI_YUSDOE_H_
#define __UAPI_YUSDOE_H_

#include <linux/types.h>
#include <linux/ioctl.h>

enum yusur_doe_cmd_opcode {
	YUSDOE_SW_RAW_CMD = 0x01,
	YUSDOE_SW_CREATE_ARRAY = 0x10,
	YUSDOE_SW_DELETE_ARRAY,
	YUSDOE_SW_CREATE_HASH,
	YUSDOE_SW_DELETE_HASH,
	YUSDOE_SW_M3_READ = 0X15,
	YUSDOE_SW_ARRAY_LOAD = 0x20,
	YUSDOE_SW_ARRAY_STORE,
	YUSDOE_SW_ARRAY_LOAD_BATCH = 0x24,
	YUSDOE_SW_ARRAY_STORE_BATCH,
	YUSDOE_SW_ARRAY_READ_CLEAR,
	YUSDOE_SW_ARRAY_WRITE,
	YUSDOE_SW_ARRAY_READ,
	YUSDOE_SW_ARRAY_READ_CLEAR_BATCH = 0x2a,
	YUSDOE_SW_HASH_INSERT = 0x30,
	YUSDOE_SW_HASH_DELETE,
	YUSDOE_SW_HASH_QUERY,
	YUSDOE_SW_HASH_INSERT_BATCH = 0x34,
	YUSDOE_SW_HASH_DELETE_BATCH,
	YUSDOE_SW_HASH_QUERY_BATCH,
};

/* parameters for table creating */
struct yusdoe_table_param {
	unsigned int index_mask;
	unsigned short dov_len;
	unsigned short key_len;		/* 0 for array table */

	bool use_cache;
	bool shared_tbl;
	uint8_t l1_cache_ways[16];
	uint8_t l2_cache_ways;
	unsigned short index_sram_size;	/* default 512 */
	unsigned char cache_ways;	/* 1 or 2 */
};

struct yusdoe_sw_cmd {
	unsigned char opcode;
	unsigned char tbl_id;
	unsigned char is_read;
	/*
	 * User space should write the number of sub-command when batch
	 * operations.
	 * Besides, The table-create command will also be splited into
	 * lots of sub-cmd, kernel will calculate the count automatically.
	 */
	unsigned int cnt;
	union {
		/* user define for debug */
		struct {
			unsigned int cmd_size;
			void *cmd;
		};
		/* single array load/store */
		struct {
			unsigned int index;
			char data[256];
		};
		/* single hash instruction */
		struct {
			char key[128];
			char value[128];
		};
		/* multi array/hash base operations */
		struct {
			void *koi_list;
			void *pair_list;
		};
		/* create table */
		struct yusdoe_table_param tbl_param;
	};
	/* The number of secceed/failed command */
	unsigned int succeed;
	unsigned int failed;
	/* 
	 * Hardware Error code. The err is set to first error code
	 * return by hardware if command is compound.
	 */
	unsigned int err;
	/* Used only for kernel */
	unsigned int koi_nr_pages;
	unsigned int pair_nr_pages;
	unsigned long koi_pages;
	unsigned long pair_pages;
};

#define YUSDOE_SEND_CMD		_IOWR('D', 2, struct yusdoe_sw_cmd)

#endif /*__UAPI_YUSDOE_H_ */
