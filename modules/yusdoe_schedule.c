#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/io.h>
#include <linux/slab.h>
#include <linux/spinlock.h>

#include "yusdoe.h"

/* Invalidate command whose parent command timeout. */
static int yusdoe_invalidate_cmd(struct yusdoe_interface *doe_if,
				 struct yusdoe_sw_cmd *cmd)
{
	struct yusdoe_desc *desc, *temp;

	llist_for_each_entry(desc, doe_if->pending_llist.first, llnode) {
		if (desc->parent ? desc->parent == cmd : desc->cmd == cmd)
			desc->invalid = 1;
	}

	list_for_each_entry_safe(desc, temp, &doe_if->cache_list, cache) {
		if (desc->parent ? desc->parent == cmd : desc->cmd == cmd) {
			list_del(&desc->cache);
			if (desc->parent)
				kfree(desc->cmd);

			dev_dbg(doe_if->yusdoe->dev, "Delete desc form cache-list with tag 0x%04x\n",
						     desc->cmd_tag);
			kfree(desc);
		}
	}

	list_for_each_entry(desc, &doe_if->work_list, list) {
		if (desc->parent ? desc->parent == cmd : desc->cmd == cmd)
			desc->invalid = 1;
	}

	return 0;
}

int yusdoe_user_cmd_context(struct yusdoe_device *yusdoe,
			    struct yusdoe_sw_cmd *cmd)
{
	struct yusdoe_interface *doe_if;
	int ret;

	/* let work to send cmd and polling event */
	if (cmd->is_read)
		doe_if = yusdoe->doe_read_if;
	else
		doe_if = yusdoe->doe_write_if;

	schedule_work(&doe_if->work);

	/* put desc to pending llist */
	ret = yusdoe_process_cmd(yusdoe, cmd);
	if (ret)
		return ret;

	/* wait for cmd over */
	ret = wait_event_interruptible_timeout(yusdoe->wait,
			(cmd->cnt == cmd->succeed + cmd->failed),
			YUSDOE_CMD_TIMEOUT);
	if (ret == -ERESTARTSYS) {
		dev_warn(yusdoe->dev, "Wait queue interrupted by a signal. cmd 0x%px\n", cmd);
		goto err;
	} else if (!ret) {
		dev_info(yusdoe->dev, "CMD timedout opc:%x tbl:%d! total=%d, suc=%d, fail=%d\n",
			 cmd->opcode, cmd->tbl_id, cmd->cnt,
			 cmd->succeed, cmd->failed);

		ret = -ETIMEDOUT;
		goto err;
	}

	return 0;
err:
	spin_lock(&doe_if->work_lock);
	yusdoe_invalidate_cmd(doe_if, cmd);
	spin_unlock(&doe_if->work_lock);

	if (cmd->opcode == YUSDOE_SW_CREATE_ARRAY ||
	    cmd->opcode == YUSDOE_SW_CREATE_HASH)
		test_and_clear_bit(cmd->tbl_id, yusdoe->tbl_bitmap);

	return ret;
}

/* If submit error, return err directly */
int yusdoe_submit_cmd(struct yusdoe_device *yusdoe, struct yusdoe_sw_cmd *cmd,
		      struct yusdoe_sw_cmd *parent,
		      int (*complete_desc)(struct yusdoe_desc *,
			      		   struct yusdoe_event *))
{
	struct yusdoe_interface *doe_if;
	struct yusdoe_desc *desc;

	desc = kzalloc(sizeof(*desc), GFP_KERNEL);
	if (!desc)
		return -ENOMEM;

	/* cnt of single cmd must initial with 1 for wakeup condition! */
	if (!parent)
		cmd->cnt = 1;

	desc->yusdoe = yusdoe;
	desc->cmd = cmd;
	desc->parent = parent;
	desc->complete_desc = complete_desc;

	/* add this cmd to pending_llist */
	if (cmd->is_read)
		doe_if = yusdoe->doe_read_if;
	else
		doe_if = yusdoe->doe_write_if;

	dev_dbg(yusdoe->dev, "Submit %s cmd opc 0x%02x tbl %d\n", doe_if->name,
						cmd->opcode, cmd->tbl_id);
	llist_add(&desc->llnode, &doe_if->pending_llist);

	return 0;
}

static int yusdoe_process_pending_llist(struct yusdoe_interface *doe_if)
{
	struct yusdoe_desc *desc, *tmp_desc;
	struct yusdoe_event_queue *eq = &doe_if->eq;
	struct yusdoe_device *yusdoe = doe_if->yusdoe;
	struct llist_node *head;

	/* Step 1: move pending llist to cache list */
	head = llist_del_all(&doe_if->pending_llist);
	if (head) {
		head = llist_reverse_order(head);
		llist_for_each_entry(desc, head, llnode) {
			if (desc->invalid) {
				/* delete invalid(timeout) cmd */
				if (desc->parent)
					kfree(desc->cmd);
				kfree(desc);
			} else {
				list_add_tail(&desc->cache, &doe_if->cache_list);
			}
		}
	}

	/* Step 2: add desc to work list if there is free space for event */
	list_for_each_entry_safe(desc, tmp_desc, &doe_if->cache_list, cache) {
		if (desc->invalid) {
			kfree(desc);
			continue;
		}

		if (!EQ_IS_FULL(eq)) {
			if (yusdoe_enqueue_cmdbuffer(doe_if, desc)) {
				/*
				 * If command buffer queue is large enough,
				 * requeue operation will be unlikely occor.
				 */
				dev_dbg(yusdoe->dev, "%s cb full!",
						doe_if->name);
				break;
			} else {
				list_del(&desc->cache);
				list_add_tail(&desc->list, &doe_if->work_list);
				EQ_MOVE_HEAD(eq);
			}
		} else {
			/*
			 * If queue depth is large enough, requeue operation
			 * will be unlikely occor.
			 */
			dev_dbg(yusdoe->dev, "%s eq full with tail %d!",
					     doe_if->name, eq->sw_tail);
			break;
		}
	}

	return 0;
}

static int yusdoe_process_working_list(struct yusdoe_interface *doe_if,
				       struct yusdoe_event *event)
{
	struct yusdoe_desc *desc, *temp;
	struct yusdoe_device *yusdoe = doe_if->yusdoe;

	list_for_each_entry_safe(desc, temp, &doe_if->work_list, list) {
		/* If event lost, the head of list must be removed! */
		if (desc->invalid) {
			list_del(&desc->list);
			if (desc->parent)
				kfree(desc->cmd);

			dev_dbg(yusdoe->dev, "Delete desc form work-list with tag 0x%04x\n",
				desc->cmd_tag);

			kfree(desc);
		/* Event desc found! */
		} else if (desc->cmd_tag == event->cmd_tag) {
			list_del(&desc->list);
			if (desc->complete_desc)
				desc->complete_desc(desc, event);
			return 0;
		}
	}

	dev_err(yusdoe->dev, "Unkown event TAG 0x%04x\n", event->cmd_tag);

	return -EIO;
}

/* This work_queue will be schedules by both ioctl_context and irq_handler */
void yusdoe_polling_work(struct work_struct *work)
{
	struct yusdoe_interface *doe_if =
		container_of(work, struct yusdoe_interface, work);
	uint64_t time_start, time_update, time_now;
	struct yusdoe_device *yusdoe = doe_if->yusdoe;
	struct yusdoe_event_queue *eq = &doe_if->eq;
	uint32_t avail, event_num;
	void *addr;
	int i, j;

	time_start = ktime_get_raw();
	time_update = time_start;
	dev_dbg(yusdoe->dev, "%s eq working! tail %d head %d\n",
			     doe_if->name, eq->sw_tail, eq->sw_head);
	while (true) {
		time_now = ktime_get_raw();
		/* Check polling timedout */
		if (time_now - time_update > POLLING_TIMEDOUT)
			break;

		/* Check if there is new events */
		avail = EQ_AVAILABLE(eq);
		if (avail)
			time_update = time_now;

		/* schedule to prevent soft lockup */
		if (time_now - time_start > POLLING_WORK_SCHEDULE_TIME) {
			schedule();
			time_start = ktime_get_raw();
			time_update = time_start;
		}

		if (avail)
			dev_dbg(yusdoe->dev, "Event Find! %d\n", avail);

		/* Recive event from hardware */
		for (i = 0; i < avail; i++) {
			addr = eq->base + eq->sw_tail * eq->entry_size;

			buffer_dump(yusdoe, doe_if->name, addr, 256);

			/* It's unlikely be locked, unless batch-cmd timeout */
			spin_lock(&doe_if->work_lock);
			if (doe_if->is_read) {
				yusdoe_process_working_list(doe_if, addr);
			} else {
				event_num = le32_to_cpu(*(uint32_t *)addr);
				addr += (4 + (event_num - 1) * sizeof(struct yusdoe_event));

				for (j = 0; j < event_num; j++) {
					yusdoe_process_working_list(doe_if, addr);
					addr -= sizeof(struct yusdoe_event);
					if (j)
						EQ_WITHDRAW_HEAD(eq);
				}
			}
			spin_unlock(&doe_if->work_lock);
			EQ_MOVE_TAIL(eq);
		}

		/*
		 * Recive cmd.
		 *
		 *   PCIe EP and CPU will both access the command buffer
		 * asynchronously, Because of the posted write, there will be
		 * a certain delay between EP being triggered for reading
		 * buffer and CPU write next command.
		 *   If we don't wait for previous command buffer DMA succeed
		 * when cb_ring is full, old command may be covered by new
		 * enqueued command.
		 */
		if (!CB_ALMOST_FULL(doe_if) && !yusdoe_dma_busy(doe_if)) {
			spin_lock(&doe_if->work_lock);
			yusdoe_process_pending_llist(doe_if);
			spin_unlock(&doe_if->work_lock);
		}

		/* Send cmd by dma */
		yusdoe_send_cmd(doe_if);
	}

	dev_dbg(yusdoe->dev, "%s eq workdone! tail %d head %d\n",
			     doe_if->name, eq->sw_tail, eq->sw_head);

	yusdoe_clean_irq(doe_if);
}
