#include <linux/slab.h>
#include "yusur_mm.h"

struct yusur_mm *yusur_mm_init(uint64_t base, uint64_t size,
			       uint32_t align_mask, const char *name)
{
	struct yusur_mm	*ymm;

	ymm = kmalloc(sizeof(*ymm), GFP_KERNEL);
	if (!ymm)
		return ERR_PTR(-ENOMEM);

	ymm->base_address = base;
	ymm->total_size = size;
	ymm->align_mask = align_mask;
	ymm->name = name;
	ymm->used_list = NULL;

	/* init the head of free_list with total size */
	ymm->free_list = kmalloc(sizeof(struct mm_region), GFP_KERNEL);
	if (!ymm->free_list) {
		kfree(ymm);
		return ERR_PTR(-ENOMEM);
	}
	ymm->free_list->address = base;
	ymm->free_list->size = size;
	ymm->free_list->next = NULL;

	return ymm;
}

void yusur_mm_uninit(struct yusur_mm *ymm)
{
	struct mm_region *mr, *prev;

	if (!ymm)
		return;

	/* traverse to free used_list node */
	mr = ymm->used_list;
	while (mr) {
		prev = mr;
		mr = prev->next;
		kfree(prev);
	}

	/* traverse to free free_list node */
	mr = ymm->free_list;
	while (mr) {
		prev = mr;
		mr = prev->next;
		kfree(prev);
	}

	kfree(ymm);
}

int64_t yusur_malloc(struct yusur_mm *ymm, uint64_t size)
{
	struct mm_region *mr, *new_mr = NULL, *prev = NULL;

	if (!ymm && !ymm->free_list)
		return -ENOMEM;

	/* let the region size align */
	size = (size + ymm->align_mask) & ~ymm->align_mask;

	/* traverse free_list to find enough space */
	mr = ymm->free_list;
	while(mr) {
		if (mr->size == size) {
			/* change the head node if first MR is used */
			if (!prev)
				ymm->free_list = mr->next;
			else
				prev->next = mr->next;

			new_mr = mr;
			break;
		} else if (mr->size > size) {
			/* alloc new MR node for allocation */
			new_mr = kmalloc(sizeof(*new_mr), GFP_KERNEL);
			if (!new_mr)
				return -ENOMEM;
			new_mr->size = size;
			new_mr->address = mr->address;

			/* decrease the size of free MR */
			mr->size -= size;
			mr->address += size;
			break;
		}

		prev = mr;
		mr = mr->next;
	}

	if (!new_mr)
		return -ENOMEM;

	/* add node to the head of used_list */
	new_mr->next = ymm->used_list;
	ymm->used_list = new_mr;

	yusur_mm_dump(ymm);

	return new_mr->address;
}

/* Insert memory region in address order */
int _try_insert(struct yusur_mm *ymm, struct mm_region *prev,
		struct mm_region *new_mr, struct mm_region *mr)
{
	/* not the first region */
	if (!prev && new_mr->address > mr->address)
		return -1;

	/* find the first MR little than new_mr */
	if (prev && new_mr->address < prev->address)
		return -1;

	/* now find the place to insert */
	new_mr->next = mr;

	/* maybe can merge with next_mr */
	if (mr && (new_mr->address + new_mr->size == mr->address)) {
		new_mr->size += mr->size;
		new_mr->next = mr->next;
		kfree(mr);
	}

	if (prev) {
		/* maybe can merge to prev_mr */
		if (prev->address + prev->size == new_mr->address) {
			prev->size += new_mr->size;
			prev->next = new_mr->next;
			kfree(new_mr);
		} else {
			prev->next = new_mr;
		}
	} else {
		/* insert as the head of free_list */
		ymm->free_list = new_mr;
	}
	
	return 0;
}

void yusur_free(struct yusur_mm *ymm, uint64_t address)
{
	struct mm_region *mr, *new_mr = NULL, *prev = NULL;

	if (!ymm)
		return;

	/* traverse used_list to find the target MR */
	new_mr = ymm->used_list;
	while (new_mr) {
		if (new_mr->address == address)
			break;
		prev = new_mr;
		new_mr = new_mr->next;
	}
	if (!new_mr) {
		pr_warn("Unknowned address to free\n");
		return;
	}

	/* delete new_mr from used list */
	if (!prev)
		ymm->used_list = new_mr->next;
	else
		prev->next = new_mr->next;

	/* traverse free_list to insert the new free_MR */
	prev = NULL;
	mr = ymm->free_list;
	while (_try_insert(ymm, prev, new_mr, mr)) {
		prev = mr;
		mr = mr->next;
	}

	yusur_mm_dump(ymm);
}
