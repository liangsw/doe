#ifndef __YUSDOE_REG_H_
#define __YUSDOE_REG_H_

/* DOE parameters */
#define YUSDOE_TBL_NUM			256
#define YUSDOE_USER_TBL_NUM		238
#define YUSDOE_SPEC_TBL_BASE		240
#define YUSDOE_CMD_MAXSIZE		0x140	/* index resource 256 Bytes */
#define YUSDOE_CMD_ALIGN		0x1f
#define YUSDOE_CMDDMA_ALIGN		0x1f
#define YUSDOE_L1CACHE_NUM		16
#define YUSDOE_L1CACHE_TAG_NUM	32
#define YUSDOE_ITEM_NUM_PER_TAG	8

#define YUSDOE_HCU_PARAM_CNT		0x10

#define YUSDOE_L2CACHE_TAG_NUM	512
#define YUSDOE_L1CACHE_SIZE		0x4000		/* 16K */
#define YUSDOE_L2CACHE_SIZE		0x40000		/* 256K */
#define YUSDOE_CACHE_ALIGN		0x1
#define YUSDOE_CACHE_TAG_ALIGN	0x0

#define YUSDOE_INDEX_SRAM_SIZE		0x1000		/* 16K */
#define YUSDOE_INDEX_SRAM_ALIGN		0x1
/* Logical address offset for index ddr resources */
#define YUSDOE_INDEX_ITEM_SIZE		256

#define YUSDOE_DDR0_SIZE		0x100000000	/* 4G */
#define YUSDOE_DDR1_SIZE		0x100000000	/* 4G */
#define YUSDOE_DDR_ALIGN		0xfff		/* 4K */

/* DOE internal register */
#define YUSDOE_REG_BAR			0
#define YUSDOE_REG_BASE			0
#define YUSDOE_VERSION			0x00
#define YUSDOE_RESET			0x04
#define YUSDOE_RD_CHANNEL_BASE		(YUSDOE_REG_BASE + 0x10)
#define YUSDOE_WR_CHANNEL_BASE		(YUSDOE_REG_BASE + 0x40)
#define YUSDOE_CMD_ADDR_LOW		0x00
#define YUSDOE_CMD_ADDR_HIGH		0x04
#define YUSDOE_CMD_LEN			0x08
#define YUSDOE_CMD_CONTROL		0x0c
#define YUSDOE_EVENT_SIZE		0x10
#define YUSDOE_EVENT_TOTAL_SIZE		0x14
#define YUSDOE_EVENT_BASE_LOW		0x18
#define YUSDOE_EVENT_BASE_HIGH		0x1c
#define YUSDOE_EVENT_PTR_LOW		0x20
#define YUSDOE_EVENT_PTR_HIGH		0x24

#define ADDITION_CMD_NUM			300

/* Interrupt vector */
enum yusdoe_irq_type {
#ifdef PLDA_VERSION
	YUSODE_IRQ_WRITE_EQ = 24,
	YUSODE_IRQ_READ_EQ = 25,
	YUSODE_IRQ_MAX = 32,
#else
	YUSODE_IRQ_WRITE_EQ = 0,
	YUSODE_IRQ_READ_EQ = 1,
	YUSODE_IRQ_MAX = 32,
#endif
};

/*
 * AIE: header + 32'B index + data
 * HIE: header + key + value
 */
struct yusdoe_hw_cmd_head {
	uint16_t cmd_tag;
	uint16_t valid;
	uint16_t cmd_len;
	uint8_t resv[26];
	uint8_t opcode;
	uint8_t table_id;
} __attribute__((packed));

enum yusdoe_status {
	/* YUSDOE_STATUS_NONE, */
	YUSDOE_STATUS_SUCCESS,
	YUSDOE_STATUS_INVALID_CMD,
};

struct yusdoe_event {
	uint8_t status;
	uint8_t resv;
	uint16_t cmd_tag;
} __attribute__((packed));

enum yusdoe_cmd_opcode {
	YUSDOE_ARRAY_LOAD = 0x20,
	YUSDOE_ARRAY_STORE,
	YUSDOE_ARRAY_READ_CLEAR = 0x26,
	YUSDOE_ARRAY_WRITE,
	YUSDOE_ARRAY_READ,
	YUSDOE_HASH_INSERT = 0x30,
	YUSDOE_HASH_DELETE,
	YUSDOE_HASH_QUERY,
};

enum yusdoe_special_table {
	YUSDOE_HASH_FLUSH_TABLE = 0xee, /* 238, Flush table for hash */
	YUSDOE_INDEX_RES_TABLE = 0xef,	/* 239, Index Resources for hash */
	YUSDOE_HCU_PARAM_TABLE = 0xf7,	/* 247, Hash Compute Unit */
	YUSDOE_CACHE_CONFIG_TABLE,
	YUSDOE_INDEX_MANAGE_TABLE,	/* 249, Index Param Manage for hash */
	YUSDOE_AIE_PARAM_TABLE = 0xfb,	/* 251, Array Instruction Engine */
	YUSDOE_HIE_PARAM_TABLE,		/* 252, Hash Instruction Engine */
	YUSDOE_MIU_PARAM_TABLE,		/* 253, Memory Interface Unit */
};

/* Memory Interface Unit */
struct yusdoe_miu_param {
	/*
	 * For array table:
	 *   item_size is equal to aie_param.item_size
	 *   item_len is equal to aie_param.data_len +
	 *   			       cache ? 8 : 0 +
	 *   			       ts ? 8 : 0
	 * For hash table:
	 *   item_size is equal to hie_param.item_size
	 *   item_len is equal to hie_param.key_len + value_len + 32, include
	 *     6 Bytes next. list for hash collision
	 *     6 Bytes prec. list for hash collision
	 *     1 Bytes flag. bit[0] valid
	 *     3 Bytes cache status.
	 *     16 Bytes hashcode.
	 */
	uint8_t item_size;
	uint16_t item_len;
	uint32_t ddr_base_low;
	uint8_t ddr_base_high : 4;
	uint8_t ddr_channel : 1;
	uint8_t : 3;
} __attribute__((packed));

/* Hash Instruction Engine */
struct yusdoe_hie_param {
	uint16_t key_len;
	uint8_t cache_ways;	/* All 4 tags are used for hash cache, must be set to 1 */
	uint32_t index_mask;	/* The depth of hash table is 2^index_mask-1 */
	union {
		uint16_t function;
		struct {
			uint16_t : 2;
			uint16_t cache_status_dis : 1;
			uint16_t : 4;
			uint16_t cache_hash_insert : 1;
			uint16_t : 7;
		};
	};
	uint16_t value_len;
	uint8_t item_size;	/* log2(miu_param.data_len) */
	uint8_t ddr_channel : 4;
	uint8_t valid : 4;
} __attribute__((packed));

/* Array Instruction Engine */
struct yusdoe_aie_param {
	union {
		uint16_t function;
		struct {
			uint16_t : 2;
			uint16_t cache_status_dis : 1;
			uint16_t : 4;
			uint16_t ts_insert : 1;
			uint16_t : 7;
		};
	};
	uint8_t cache_ways;	/* The number of 32bits tag resource, only support 1 or 2 */
	uint32_t index_mask;	/* The depth of array table is 2^index_mask-1 */
	uint16_t data_len;
	uint8_t item_size;	/* log2(miu_param.data_len) */
	uint8_t ddr_channel : 4;
	uint8_t valid: 4;
} __attribute__((packed));

struct yusdoe_cache_config {
	uint8_t		valid : 2;
	uint8_t		tag_type : 3;
	uint8_t		tbl_type : 3;
	uint16_t	tag_base;
	uint16_t	tag_num;
	uint16_t	data_base;
	uint16_t	data_len;
	uint8_t		data_size;
	uint32_t	op_index_mask;
} __attribute__((packed));

struct yusdoe_index_param {
	uint16_t ram_physic_base;
	/* ddr_base is the index of resource table, base 256Bytes */
	uint32_t ddr_physic_base;
	/* point and state: SW init to 0, HW maintain */
	uint16_t ram_point;
	uint32_t ddr_point;
	uint8_t ddr_state;
} __attribute__((packed));

/*
 * Hash Compute Unit
 *
 *   This parameter is global for all hash table. For other cmd[5:2] index, it
 * represents the table id, here it represents the hcu_instruct of every eight
 * bytes of key. The maximum key length is 128, so the highest index is 15.
 */
struct yusdoe_hcu_param {
	uint32_t instruct;
};

#endif /* __YUSDOE_REG_H_ */
