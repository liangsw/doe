%define ver 1.4

%define rel 1
%define srcname HADOS_doe
%define sdkdir /opt/yusur/hados
%if "%{?platform}" == "soc"
%define Soc .soc
%endif

Name: hados_doe
Version: %{ver}
Release: %{rel}%{?dist}%{?Soc}
Group: YUSUR
License: BSD and LGPLv2 and GPLv2
Summary: Driver of yusur data offload engine

Source: HADOS_doe.tar.gz
BuildRequires: gcc, make,
%if 0%{?ubuntu_version}
BuildRequires: libelf-dev
%{!?__with_new_kernel: Requires(pre): linux-headers-%{?kernel_version}-generic}
%else
BuildRequires: elfutils-libelf-devel, kernel-devel
%endif
ExclusiveArch: x86_64 aarch64
ExclusiveOs: linux

%prep
%setup -q -n %{srcname}

%build
cd modules && make && cd -
cd lib && make && cd -

%install
cd modules && %make_install && cd -
cd lib && %make_install && cd -

%post

%preun

%description
Provide DOE configuration API to OVS(DPDK), include table creation, table
deletion and data load/store/delete operations.

%files
%{sdkdir}/modules/hados_doe.ko
%{sdkdir}/include/hados_doe.h
%{sdkdir}/lib/libhados_doe.so
