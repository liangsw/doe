/**
 * @brief libhados_doe 数组表API示例
 * @example hados_doe_array.c
 */

#include "hados_doe.h"
#include "stdio.h"
#include <string.h>

/// @cond
struct pair_item {
	int32_t index;
	char    data[32];
};
/// @endcond

int main(int argc, char *argv[])
{
	int fd, i, success_count;
	doe_result_t ret;
	uint8_t tbl_id = 1;
	char buf[64];
	struct pair_item pair_list[10];
	int32_t index_list[10];

	tbl_id = 1;
	// 创建doe句柄
	fd = hados_doe_request_fd();
	if (fd == -1) {
		fprintf(stderr, "Open DOE failed!\n");
		return -1;
	}

	// 创建表号为0的array表，表深度为64, 数据长度为32字节
	ret = hados_doe_create_arraytbl(fd, tbl_id, 64, 32, NULL);
	if (ret != DOE_RESULT_SUCCESS) {
		fprintf(stderr, "call hados_doe_create_arraytbl failed. errcode:%d\n", ret);
		return -1;
	}

	do {
		// 在索引1位置插入数据
		ret = hados_doe_array_store(fd, tbl_id, 1, "hello", sizeof("hello"));
		if (ret != DOE_RESULT_SUCCESS) {
			fprintf(stderr, "call hados_doe_array_store failed. errcode:%d\n", ret);
			break;
		}

		// 读取索引1位置的数据
		ret = hados_doe_array_load(fd, tbl_id, 1, buf, sizeof(buf));
		if (ret != DOE_RESULT_SUCCESS) {
			fprintf(stderr, "call hados_doe_array_load failed. errcode:%d\n", ret);
			break;
		}

		// 清空索引1位置的数据
		ret = hados_doe_array_read_clear(fd, tbl_id, 1, buf, sizeof(buf));
		if (ret != DOE_RESULT_SUCCESS) {
			fprintf(stderr, "call hados_doe_array_read_clear failed. err:%d\n",
				ret);
			break;
		}

		// 批量存储数据
		for (i = 0; i < 10; ++i) {
			pair_list[i].index = i;
			memcpy(pair_list[i].data, "hello", sizeof("hello"));
		}

		success_count = hados_doe_array_store_batch(fd, tbl_id, 10, pair_list);
		if (success_count != 10) {
			fprintf(stderr, "call hados_doe_array_store_batch failed. succ: %d\n",
				success_count);
			break;
		}

		// 批量读取
		memset(pair_list, 0, sizeof(struct pair_item) * 10);
		for (i = 0; i < 10; ++i) {
			index_list[i] = i;
		}

		success_count = hados_doe_array_load_batch(fd, tbl_id, 10, index_list, pair_list);
		if (success_count != 10) {
			fprintf(stderr, "call hados_doe_array_load_batch failed. succ: %d\n",
				success_count);
			break;
		}

		// 批量读清
		memset(pair_list, 0, sizeof(struct pair_item) * 10);
		success_count = hados_doe_array_readclear_batch(fd, tbl_id, 10, index_list,
			pair_list);
		if (success_count != 10) {
			fprintf(stderr, "call hados_doe_array_readclear_batch failed. succ: %d\n",
				success_count);
			break;
		}
	} while (0);

	// 删除表
	ret = hados_doe_delete_arraytbl(fd, tbl_id);
	if (ret != DOE_RESULT_SUCCESS) {
		fprintf(stderr, "call hados_doe_delete_arraytbl failed. errcode:%d\n", ret);
		return -1;
	}

	hados_doe_release_fd(fd);

	return 0;
}
