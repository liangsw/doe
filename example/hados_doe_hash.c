/**
 * @brief libhados_doe 哈希表API示例
 * @example hados_doe_hash.c
 */

#include "hados_doe.h"
#include "stdio.h"
#include <string.h>

/// @cond
struct pair_item {
	char    key[32];
	char    value[32];
};

struct index_item {
	char key[32];
};
/// @endcond

int main(int argc, char *argv[])
{
	int success_count, i, fd;
	uint8_t tbl_id;
	doe_result_t ret;
	char buf[64];
	struct pair_item pair_list[10];
	struct index_item index_list[10];

	tbl_id = 3;
	// 创建doe句柄
	fd = hados_doe_request_fd();
	if (fd == -1) {
		fprintf(stderr, "Open DOE failed!\n");
		return -1;
	}

	// 创建hash表，表深度为64, key长度为32，value长度为32
	ret = hados_doe_create_hashtbl(fd, tbl_id, 64, 32, 32, NULL);
	if (ret != DOE_RESULT_SUCCESS) {
		fprintf(stderr, "call hados_doe_create_hashtbl failed. errcode:%d\n", ret);
		return -1;
	}

	do {
		// 插入数据[hello, world]
		ret = hados_doe_hash_insert(fd, tbl_id, "hello", sizeof("hello"),
			"world", sizeof("world"));
		if (ret != DOE_RESULT_SUCCESS) {
			fprintf(stderr, "call hados_doe_hash_insert failed. errcode:%d\n", ret);
			break;
		}

		// 读取key为hello对应的值
		ret = hados_doe_hash_query(fd, tbl_id, "hello", sizeof("hello"),
			buf, sizeof(buf));
		if (ret != DOE_RESULT_SUCCESS) {
			fprintf(stderr, "call hados_doe_hash_query failed. errcode:%d\n", ret);
			break;
		}

		// 读取key为hello的数据
		ret = hados_doe_hash_delete(fd, tbl_id, "hello", sizeof("hello"));
		if (ret != DOE_RESULT_SUCCESS) {
			fprintf(stderr, "call hados_doe_hash_delete failed. errcode:%d\n", ret);
			break;
		}

		// 批量插入
		for (i = 0; i < 10; ++i) {
			memcpy(pair_list[i].key, "hello", sizeof("hello"));
			// 这里只是为了测试在hello后面加了1到9作为key值区分
			pair_list[i].key[strlen("hello")] = i + 48;
			memcpy(pair_list[i].value, "world", sizeof("world"));
		}

		printf("after insert\n");

		success_count = hados_doe_hash_insert_batch(fd, tbl_id, 10, pair_list);
		if (success_count != 10) {
			fprintf(stderr, "call hados_doe_hash_insert_batch failed. succ: %d\n",
				success_count);
			break;
		}

		// 批量查询
		memset(pair_list, 0, sizeof(struct pair_item) * 10);
		for (int i = 0; i < 10; ++i) {
			memcpy(index_list[i].key, "hello", sizeof("hello"));
			index_list[i].key[strlen("hello")] = i + 48;
		}

		success_count = hados_doe_hash_query_batch(fd, tbl_id, 10, index_list, pair_list);
		if (success_count != 10) {
			fprintf(stderr, "call hados_doe_hash_query_batch failed. succ: %d\n",
				success_count);
			break;
		}

		// 批量删除
		success_count = hados_doe_hash_delete_batch(fd, tbl_id, 10, index_list);
		if (success_count != 10) {
			fprintf(stderr, "call hados_doe_hash_delete_batch failed. succ: %d\n",
				success_count);
			break;
		}
	} while (0);

	// 删除表
	ret = hados_doe_delete_hashtbl(fd, tbl_id);
	if (ret != DOE_RESULT_SUCCESS) {
		fprintf(stderr, "call hados_doe_delete_hashtbl failed. errcode:%d\n", ret);
		return -1;
	}

	hados_doe_release_fd(fd);

	return 0;
}
