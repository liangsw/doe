/*
 * @Author: Xing Wang (wangx_ddpt@yusur.tech)
 * @date: 2022-12-09 17:07:53
 * @last_author: Xing Wang (wangx_ddpt@yusur.tech)
 * @last_edit_time: 2022-12-12 11:42:48
 */

#define ANKERL_NANOBENCH_IMPLEMENT
#include "nanobench.h"

#define DOCTEST_CONFIG_IMPLEMENT
#include "doctest.h"

#include "uapi_yusdoe.h"

extern "C" {
#include "hados_doe.h"
}

#include <iostream>
#include <string>
#include <memory>
#include <argp.h>
#include <unordered_map>

static char value[] = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";

template <uint8_t DATA_LEN>
class array_batch_test
{
	struct item_t {
		uint32_t    index;
		char        data[DATA_LEN];
	};

	struct index_t {
		int index;
	};

public:
	array_batch_test(int fd, uint8_t tbl_id, uint32_t depth)
		: _fd(fd), _tbl_id(tbl_id), _depth(depth)
	{}

	int init_env()
	{
		/* malloc data for batch insert */
		_data = (item_t *)malloc(sizeof(item_t) * _depth);
		memset(_data, 0, sizeof(item_t) * _depth);

		/* malloc index for batch load */
		_indexs = (uint32_t*)malloc(sizeof(uint32_t) * _depth);

		/* init data and index */
		for (uint32_t i = 0; i < _depth; ++i) {
			_indexs[i] = i;
			_data[i].index = i;
			memcpy(_data[i].data, "hello", sizeof("hello"));
		}

		/* create table */
		auto ret = hados_doe_create_arraytbl(_fd, _tbl_id, _depth, DATA_LEN, NULL);
		if (ret != DOE_RESULT_SUCCESS) {
			std::cout << "create array table failed. ret=" << ret << std::endl;
	}

	return ret;
}

void clear_env()
{
	hados_doe_delete_arraytbl(_fd, _tbl_id);

	free(_data);
	free(_indexs);
}

bool store(bool random_index, const std::string &msg)
{
	bool flag = true;
	uint32_t index = 0;
	uint32_t array_index[1000];

	if (!random_index) {
		for (auto i = 0; i < 1000; ++i)
			array_index[i] = i;
	} else {
		ankerl::nanobench::Rng rng;
		for (auto i = 0; i < 1000; ++i) {
			array_index[i] = rng()%1000;
		}
	}

	ankerl::nanobench::Bench().minEpochIterations(50).run(msg, [&] {
		if (hados_doe_array_store(_fd, _tbl_id, array_index[index++], _data, DATA_LEN))
		flag = false;
	});

	return flag;
}

bool batch_store(uint32_t count, const std::string& msg)
{
	int total = 0, err = 0;

	ankerl::nanobench::Bench().minEpochIterations(10).run(msg, [&] {
		if (hados_doe_array_store_batch(_fd, _tbl_id, count, _data) != count) {
			err++;
		}
		total++;
	});

	// std::cout << "excute count: " << total << ". err count: " << err << std::endl;
	// std::cout << std::endl;

	return !err;
}

bool load(bool random_index, const std::string& msg)
{
	bool flag = true;
	uint32_t index = 0;
	uint32_t array_index[1000];

	if (!random_index) {
		for (auto i = 0; i < 1000; ++i)
			array_index[i] = i;
	} else {
		ankerl::nanobench::Rng rng;
		for (auto i = 0; i < 1000; ++i) {
			array_index[i] = rng()%1000;
		}
	}

	ankerl::nanobench::Bench().minEpochIterations(50).run(msg, [&] {
		if (hados_doe_array_load(_fd, _tbl_id, array_index[index++], _data, DATA_LEN))
		flag = false;
	});

	return flag;
}

bool read_clear(bool random_index, const std::string& msg)
{
	bool flag = true;
	uint32_t index = 0;
	uint32_t array_index[1000];

	if (!random_index) {
		for (auto i = 0; i < 1000; ++i)
			array_index[i] = i;
	} else {
		ankerl::nanobench::Rng rng;
		for (auto i = 0; i < 1000; ++i) {
			array_index[i] = rng()%1000;
		}
	}

	ankerl::nanobench::Bench().minEpochIterations(50).run(msg, [&] {
		if (hados_doe_array_read_clear(_fd, _tbl_id, array_index[index++], _data, DATA_LEN))
			flag = false;
	});

	return flag;
}

bool batch_load(uint32_t count, const std::string& msg)
{
	int total = 0, err = 0;

	auto tmp = std::make_unique<item_t[]>(_depth);
	ankerl::nanobench::Bench().minEpochIterations(8).run(msg, [&] {
		if (hados_doe_array_load_batch(_fd, _tbl_id, count, _indexs, tmp.get()) != count) {
			err++;
		}
		total++;
	});

	// std::cout << "excute count: " << total << ". err count: " << err << std::endl;
	// std::cout << std::endl;

	return !err;
}

bool batch_readclear(uint32_t count, const std::string& msg)
{
	int total = 0, err = 0;

	auto tmp = std::make_unique<item_t[]>(_depth);
	ankerl::nanobench::Bench().minEpochIterations(8).run(msg, [&] {
		if (hados_doe_array_readclear_batch(_fd, _tbl_id, count, _indexs, tmp.get()) != count) {
			err++;
		}
		total++;
	});

	// std::cout << "excute count: " << total << ". err count: " << err << std::endl;
	// std::cout << std::endl;

	return !err;
}

private:
	item_t*     _data;
	uint32_t*   _indexs;
	int         _fd;
	uint8_t     _tbl_id;
	uint32_t    _depth;
};

template <uint8_t KEY_LEN, uint8_t VAL_LEN>
class hash_batch_test
{
	struct item_t
	{
		char key[KEY_LEN];
		char val[VAL_LEN];
	} __attribute__((packed));

	struct key_t {
	char key[KEY_LEN];
	} __attribute__((packed));

private:
	item_t*     _data;
	char*      _keys;
	int         _fd;
	uint8_t     _tbl_id;
	uint32_t    _depth;
	bool        _init = true;

public:
	hash_batch_test(int fd, uint8_t tbl_id, uint32_t depth)
	: _fd(fd), _tbl_id(tbl_id), _depth(depth)
	{
		_data = new item_t[_depth];
		memset(_data, 0, sizeof(item_t) * _depth);

		// _keys = new key_t[_depth];
		_keys = (char *)malloc(KEY_LEN * _depth);
		memset(_keys, 0, KEY_LEN * _depth);


		for (uint32_t i = 0; i < _depth; ++i) {
			std::string tmp("hello");
			tmp += std::to_string(i);

			memcpy(_data[i].key, tmp.c_str(), tmp.length());
			memcpy(_keys + KEY_LEN * i, tmp.c_str(), tmp.length());

			strcpy(_data[i].val, "world");
		}
	}

	~hash_batch_test()
	{
		delete[] _data;
		free(_keys);
	}

	int init_env(bool init_data = false)
	{
		doe_result_t ret = hados_doe_create_hashtbl(_fd, _tbl_id, _depth, KEY_LEN, VAL_LEN, NULL);
		if (ret != DOE_RESULT_SUCCESS) {
			fprintf(stderr, "call yusdoe_create_hashtbl failed. _fd=%d, _tbl_id=%d, _depth=%d, key_len=%d, val_len=%d\n",
				_fd, _tbl_id, _depth, KEY_LEN, VAL_LEN);
			return ret;
		}

		int32_t count = 8000000;
		if (init_data) {
			int success = hados_doe_hash_insert_batch(_fd, _tbl_id, count, _data);
			if (success != count) {
				std::cout << "insert data falied. success=" << success << ", count=" << count << std::endl;
				ret = DOE_RESULT_TBL_ERR;
			}
		}

		return ret;
	}

	void clear_env()
	{
		hados_doe_delete_hashtbl(_fd, _tbl_id);
	}

	bool insert(const std::string& msg)
	{
		int err = 0, index = 0;

		init_env();

		ankerl::nanobench::Bench().minEpochIterations(8).run(msg, [&] {
			if (hados_doe_hash_insert(_fd, _tbl_id, _keys + index * KEY_LEN, KEY_LEN, value, VAL_LEN) != DOE_RESULT_SUCCESS) {
				err++;
			}
			index++;
		});

		clear_env();

		return !err;
	}

	bool batch_insert(uint32_t count, const std::string& msg)
	{
		init_env();

		int total = 0, err = 0, index = 0;

		ankerl::nanobench::Bench().minEpochIterations(8).run(msg, [&] {
			if (hados_doe_hash_insert_batch(_fd, _tbl_id, count, _data + index) != count) {
				err++;
			}
			total++;
			index += count;
		});

		std::cout << "excute count: " << total << ". err count: " << err << std::endl;
		std::cout << std::endl;
		clear_env();

		return !err;
	}

	bool query(const std::string &msg)
	{
		int total = 0, err = 0, index = 0;

		init_env(true);

		char buf[256];
		ankerl::nanobench::Bench().minEpochIterations(8).run(msg, [&] {
			if (hados_doe_hash_query(_fd, _tbl_id, _keys + index * KEY_LEN, KEY_LEN, buf, 255) != DOE_RESULT_SUCCESS) {
				err++;
			}
			total++;
		});

		clear_env();

		return !err;
	}

	bool batch_query(uint32_t count, const std::string& msg)
	{
		uint32_t total = 0, err = 0, ret;

		auto tmp = std::make_unique<item_t[]>(_depth);
		ankerl::nanobench::Bench().minEpochIterations(8).run(msg, [&] {
			ret = hados_doe_hash_query_batch(_fd, _tbl_id, count, _keys, tmp.get());
			if (ret != count) {
				err++;
			}
			total++;
		});

		// std::cout << "excute count: " << total << ". err count: " << err << ", ret=" << ret << std::endl;
		// std::cout << std::endl;

		return !err;
	}

	bool del(const std::string& msg)
	{
		int err = 0, index = 0, ret;

		if (!init_env(true)) {
			ankerl::nanobench::Bench().minEpochIterations(1).run(msg, [&] {
				ret = hados_doe_hash_delete(_fd, _tbl_id, _keys + index * KEY_LEN, KEY_LEN);
				if (ret != DOE_RESULT_SUCCESS) {
					err++;
				}
				index++;
			});

			std::cout << "excute count: " << index << ". err count: " << err << std::endl;
			std::cout << std::endl;
		}

		clear_env();

		return !err;
	}

	bool batch_del(int32_t count, const std::string &msg)
	{
		int err = 0, ret, index = 0;

		if (!init_env(true)) {
			ankerl::nanobench::Bench().minEpochIterations(8).run(msg, [&] {
				ret = hados_doe_hash_delete_batch(_fd, _tbl_id, count, _keys + index * KEY_LEN);
				if (ret != count) {
					err++;
				}
				index += count;
			});

			std::cout << "excute count: " << index << ". err count: " << err << std::endl;
			std::cout << std::endl;
		}

		return !err;
	}
};


static struct argp_option options[] = {
	{"op",		'o',	"string",		0, "array table perf test: store load readclear batch_store batch_load batch_readclear"},
	{ 0 },
};

struct arguments {
	std::string test_doc;
};

struct arguments argu;

static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
	struct arguments *argu = (struct arguments *)(state->input);

	switch (key) {
	case 'o':
		fprintf(stderr, "a: arg=%s\n", arg);
		argu->test_doc = std::string(arg);
		break;
	default:
		break;
	}

	return 0;
}

/* argp parser. */
static struct argp argp = {
	.options = options,
	.parser = parse_opt,
	.args_doc = NULL,
	.doc = NULL,
	.children = NULL,
	.help_filter = NULL,
	.argp_domain = NULL,
};

void array_store_test()
{
	int fd = hados_doe_request_fd();
	if (fd == -1) {
		std::cout << "call yusdoe_request_fd failed." << std::endl;
		return;
	}

	uint8_t tbl_id = 1;
	uint32_t tbl_deepth = 100000;

	auto array32 = array_batch_test<32>(fd, tbl_id, tbl_deepth);
	array32.init_env();
	array32.store(true, "random index");
    	array32.store(false, "not random index");
	array32.clear_env();

    	auto array64 = array_batch_test<64>(fd, tbl_id, tbl_deepth);
	array64.init_env();
	CHECK(array64.store(true, "random index"));
    	CHECK(array64.store(false, "not random index"));
	array64.clear_env();

    	auto array128 = array_batch_test<128>(fd, tbl_id, tbl_deepth);
	array128.init_env();
	CHECK(array128.store(true, "random index"));
    	CHECK(array128.store(false, "not random index"));
	array128.clear_env();

    	hados_doe_release_fd(fd);
}

void array_load_test()
{
	int fd = hados_doe_request_fd();
	if (fd == -1) {
		std::cout << "call yusdoe_request_fd failed." << std::endl;
		return;
	}

	uint8_t tbl_id = 1;
	uint32_t tbl_deepth = 10000000;

	auto array32 = array_batch_test<32>(fd, tbl_id, tbl_deepth);
	array32.init_env();
	CHECK(array32.load(true, "random index"));
    	CHECK(array32.load(false, "not random index"));
	array32.clear_env();

    	auto array64 = array_batch_test<64>(fd, tbl_id, tbl_deepth);
	array64.init_env();
	CHECK(array64.load(true, "random index"));
    	CHECK(array64.load(false, "not random index"));
	array64.clear_env();

    	auto array128 = array_batch_test<128>(fd, tbl_id, tbl_deepth);
	array128.init_env();
	CHECK(array128.load(true, "random index"));
    	CHECK(array128.load(false, "not random index"));
	array128.clear_env();

    	hados_doe_release_fd(fd);
}

void array_readclear_test()
{
	int fd = hados_doe_request_fd();
	if (fd == -1) {
		std::cout << "call yusdoe_request_fd failed." << std::endl;
		return;
	}

	uint8_t tbl_id = 1;
	uint32_t tbl_deepth = 10000000;

	auto array32 = array_batch_test<32>(fd, tbl_id, tbl_deepth);
	array32.init_env();
	CHECK(array32.read_clear(true, "random index"));
	CHECK(array32.read_clear(false, "not random index"));
	array32.clear_env();

	auto array64 = array_batch_test<64>(fd, tbl_id, tbl_deepth);
	array64.init_env();
	CHECK(array64.read_clear(true, "random index"));
	CHECK(array64.read_clear(false, "not random index"));
	array64.clear_env();

	auto array128 = array_batch_test<128>(fd, tbl_id, tbl_deepth);
	array128.init_env();
	CHECK(array128.read_clear(true, "random index"));
	CHECK(array128.read_clear(false, "not random index"));
	array128.clear_env();

	hados_doe_release_fd(fd);
}

void array_batch_store_test()
{
    int fd = hados_doe_request_fd();
    if (fd == -1) {
        std::cout << "call yusdoe_request_fd failed." << std::endl;
        return;
    }

    uint8_t tbl_id = 1;
    uint32_t tbl_deepth = 10000000;

    auto array32 = array_batch_test<32>(fd, tbl_id, tbl_deepth);
    array32.init_env();
    CHECK(array32.batch_store(100, "store 100 one time"));
    CHECK(array32.batch_store(1000, "store 1000 one time"));
    CHECK(array32.batch_store(10000, "store 10000 one time"));
    CHECK(array32.batch_store(20000, "store 20000 one time"));
    CHECK(array32.batch_store(40000, "store 40000 one time"));
    CHECK(array32.batch_store(60000, "store 60000 one time"));
    CHECK(array32.batch_store(80000, "store 80000 one time"));
    CHECK(array32.batch_store(100000, "store 100000 one time"));
    array32.clear_env();

    auto array64 = array_batch_test<64>(fd, tbl_id, tbl_deepth);
    array64.init_env();
    CHECK(array64.batch_store(100, "store 100 one time"));
    CHECK(array64.batch_store(1000, "store 1000 one time"));
    CHECK(array64.batch_store(10000, "store 10000 one time"));
    CHECK(array64.batch_store(20000, "store 20000 one time"));
    CHECK(array64.batch_store(40000, "store 40000 one time"));
    CHECK(array64.batch_store(60000, "store 60000 one time"));
    CHECK(array64.batch_store(80000, "store 80000 one time"));
    CHECK(array64.batch_store(100000, "store 100000 one time"));
    array64.clear_env();

    auto array128 = array_batch_test<128>(fd, tbl_id, tbl_deepth);
    array128.init_env();
    CHECK(array128.batch_store(100, "store 100 one time"));
    CHECK(array128.batch_store(1000, "store 1000 one time"));
    CHECK(array128.batch_store(10000, "store 10000 one time"));
    CHECK(array128.batch_store(20000, "store 20000 one time"));
    CHECK(array128.batch_store(40000, "store 40000 one time"));
    CHECK(array128.batch_store(60000, "store 60000 one time"));
    CHECK(array128.batch_store(80000, "store 80000 one time"));
    CHECK(array128.batch_store(100000, "store 100000 one time"));
    array128.clear_env();

    hados_doe_release_fd(fd);
}

void array_batch_load_test()
{
	int fd = hados_doe_request_fd();
	if (fd == -1) {
		std::cout << "call yusdoe_request_fd failed." << std::endl;
		return;
	}

	uint8_t tbl_id = 1;
	uint32_t tbl_deepth = 10000000;

	auto array32 = array_batch_test<32>(fd, tbl_id, tbl_deepth);
	array32.init_env();
	CHECK(array32.batch_load(100, "store 100 one time"));
	CHECK(array32.batch_load(1000, "store 1000 one time"));
	CHECK(array32.batch_load(10000, "store 10000 one time"));
	CHECK(array32.batch_load(20000, "store 20000 one time"));
	CHECK(array32.batch_load(40000, "store 40000 one time"));
	CHECK(array32.batch_load(60000, "store 60000 one time"));
	CHECK(array32.batch_load(80000, "store 80000 one time"));
	CHECK(array32.batch_load(100000, "store 100000 one time"));
	array32.clear_env();

	auto array64 = array_batch_test<64>(fd, tbl_id, tbl_deepth);
	array64.init_env();
	CHECK(array64.batch_load(100, "store 100 one time"));
	CHECK(array64.batch_load(1000, "store 1000 one time"));
	CHECK(array64.batch_load(10000, "store 10000 one time"));
	CHECK(array64.batch_load(20000, "store 20000 one time"));
	CHECK(array64.batch_load(40000, "store 40000 one time"));
	CHECK(array64.batch_load(60000, "store 60000 one time"));
	CHECK(array64.batch_load(80000, "store 80000 one time"));
	CHECK(array64.batch_load(100000, "store 100000 one time"));
	array64.clear_env();

	auto array128 = array_batch_test<128>(fd, tbl_id, tbl_deepth);
	array128.init_env();
	CHECK(array128.batch_load(100, "store 100 one time"));
	CHECK(array128.batch_load(1000, "store 1000 one time"));
	CHECK(array128.batch_load(10000, "store 10000 one time"));
	CHECK(array128.batch_load(20000, "store 20000 one time"));
	CHECK(array128.batch_load(40000, "store 40000 one time"));
	CHECK(array128.batch_load(60000, "store 60000 one time"));
	CHECK(array128.batch_load(80000, "store 80000 one time"));
	CHECK(array128.batch_load(100000, "store 100000 one time"));
	array128.clear_env();

	hados_doe_release_fd(fd);
}

void array_batch_readclear_test()
{
	int fd = hados_doe_request_fd();
	if (fd == -1) {
		std::cout << "call yusdoe_request_fd failed." << std::endl;
		return;
	}

	uint8_t tbl_id = 1;
	uint32_t tbl_deepth = 2000000;

	auto array32 = array_batch_test<32>(fd, tbl_id, tbl_deepth);
	array32.init_env();
	CHECK(array32.batch_readclear(100, "store 100 one time"));
	CHECK(array32.batch_readclear(1000, "store 1000 one time"));
	CHECK(array32.batch_readclear(10000, "store 10000 one time"));
	CHECK(array32.batch_readclear(20000, "store 20000 one time"));
	CHECK(array32.batch_readclear(40000, "store 40000 one time"));
	CHECK(array32.batch_readclear(60000, "store 60000 one time"));
	CHECK(array32.batch_readclear(80000, "store 80000 one time"));
	CHECK(array32.batch_readclear(100000, "store 100000 one time"));
	array32.clear_env();

	auto array64 = array_batch_test<64>(fd, tbl_id, tbl_deepth);
	array64.init_env();
	CHECK(array64.batch_readclear(100, "store 100 one time"));
	CHECK(array64.batch_readclear(1000, "store 1000 one time"));
	CHECK(array64.batch_readclear(10000, "store 10000 one time"));
	CHECK(array64.batch_readclear(20000, "store 20000 one time"));
	CHECK(array64.batch_readclear(40000, "store 40000 one time"));
	CHECK(array64.batch_readclear(60000, "store 60000 one time"));
	CHECK(array64.batch_readclear(80000, "store 80000 one time"));
	CHECK(array64.batch_readclear(100000, "store 100000 one time"));
	array64.clear_env();

	auto array128 = array_batch_test<128>(fd, tbl_id, tbl_deepth);
	array128.init_env();
	CHECK(array128.batch_readclear(100, "store 100 one time"));
	CHECK(array128.batch_readclear(1000, "store 1000 one time"));
	CHECK(array128.batch_readclear(10000, "store 10000 one time"));
	CHECK(array128.batch_readclear(20000, "store 20000 one time"));
	CHECK(array128.batch_readclear(40000, "store 40000 one time"));
	CHECK(array128.batch_readclear(60000, "store 60000 one time"));
	CHECK(array128.batch_readclear(80000, "store 80000 one time"));
	CHECK(array128.batch_readclear(100000, "store 100000 one time"));
	array128.clear_env();

	hados_doe_release_fd(fd);
}

void hash_insert_test()
{
	int fd = hados_doe_request_fd();
	if (fd == -1) {
		std::cout << "call yusdoe_request_fd failed." << std::endl;
		return;
	}

	uint8_t tbl_id = 1;
	uint32_t tbl_deepth = 10000000;

	auto hash32_32 = hash_batch_test<32, 32>(fd, tbl_id, tbl_deepth);
	CHECK(hash32_32.insert("32-32"));

	auto hash32_64 = hash_batch_test<32, 64>(fd, tbl_id, tbl_deepth);
	CHECK(hash32_64.insert("32-64"));

	auto hash32_128 = hash_batch_test<32, 128>(fd, tbl_id, tbl_deepth);
	CHECK(hash32_128.insert("32-128"));

	hados_doe_release_fd(fd);
}

void hash_query_test()
{
	int fd = hados_doe_request_fd();
	if (fd == -1) {
		std::cout << "call yusdoe_request_fd failed." << std::endl;
		return;
	}

	uint8_t tbl_id = 1;
	uint32_t tbl_deepth = 10000000;

	auto hash32_32 = hash_batch_test<32, 32>(fd, tbl_id, tbl_deepth);
	CHECK(hash32_32.query("32-32"));

	auto hash32_64 = hash_batch_test<32, 64>(fd, tbl_id, tbl_deepth);
	CHECK(hash32_64.query("32-64"));

	auto hash32_128 = hash_batch_test<32, 128>(fd, tbl_id, tbl_deepth);
	CHECK(hash32_128.query("32-128"));

	hados_doe_release_fd(fd);
}

void hash_delete_test()
{
	int fd = hados_doe_request_fd();
	if (fd == -1) {
		std::cout << "call yusdoe_request_fd failed." << std::endl;
		return;
	}

	uint8_t tbl_id = 1;
	uint32_t tbl_deepth = 10000000;

	auto hash32_32 = hash_batch_test<32, 32>(fd, tbl_id, tbl_deepth);
	hash32_32.del("32 bytes");

	auto hash32_64 = hash_batch_test<32, 64>(fd, tbl_id + 1, tbl_deepth);
	hash32_64.del("64 bytes");

	auto hash32_128 = hash_batch_test<32, 128>(fd, tbl_id + 2, tbl_deepth);
	hash32_128.del("128 bytes");

	hados_doe_release_fd(fd);
}

void hash_batch_insert_test()
{
	int fd = hados_doe_request_fd();
	if (fd == -1) {
		std::cout << "call yusdoe_request_fd failed." << std::endl;
		return;
	}

	uint8_t tbl_id = 1;
	uint32_t tbl_deepth = 10000000;

	auto hash32_32 = hash_batch_test<32, 32>(fd, tbl_id, tbl_deepth);
	CHECK(hash32_32.batch_insert(100, "100"));
	CHECK(hash32_32.batch_insert(1000, "1000"));
	CHECK(hash32_32.batch_insert(10000, "10000"));
	CHECK(hash32_32.batch_insert(20000, "20000"));
	CHECK(hash32_32.batch_insert(40000, "40000"));
	CHECK(hash32_32.batch_insert(60000, "60000"));
	CHECK(hash32_32.batch_insert(80000, "80000"));

	auto hash32_64 = hash_batch_test<32, 64>(fd, tbl_id, tbl_deepth);
	CHECK(hash32_64.batch_insert(100, "100"));
	CHECK(hash32_64.batch_insert(1000, "1000"));
	CHECK(hash32_64.batch_insert(10000, "10000"));
	CHECK(hash32_64.batch_insert(20000, "20000"));
	CHECK(hash32_64.batch_insert(40000, "40000"));
	CHECK(hash32_64.batch_insert(60000, "60000"));
	CHECK(hash32_64.batch_insert(80000, "80000"));

	auto hash32_128 = hash_batch_test<32, 128>(fd, tbl_id, tbl_deepth);
	CHECK(hash32_128.batch_insert(100, "100"));
	CHECK(hash32_128.batch_insert(1000, "1000"));
	CHECK(hash32_128.batch_insert(10000, "10000"));
	CHECK(hash32_128.batch_insert(20000, "20000"));
	CHECK(hash32_128.batch_insert(40000, "40000"));
	CHECK(hash32_128.batch_insert(60000, "60000"));
	CHECK(hash32_128.batch_insert(80000, "80000"));

	hados_doe_release_fd(fd);
}

void hash_batch_query_test()
{
	int fd = hados_doe_request_fd();
	if (fd == -1) {
		std::cout << "call yusdoe_request_fd failed." << std::endl;
		return;
	}

	uint8_t tbl_id = 1;
	uint32_t tbl_deepth = 10000000;

	auto hash32_32 = hash_batch_test<32, 32>(fd, tbl_id, tbl_deepth);
	hash32_32.init_env(true);
	CHECK(hash32_32.batch_query(100, "100"));
	CHECK(hash32_32.batch_query(1000, "1000"));
	CHECK(hash32_32.batch_query(10000, "10000"));
	CHECK(hash32_32.batch_query(20000, "20000"));
	CHECK(hash32_32.batch_query(40000, "40000"));
	CHECK(hash32_32.batch_query(60000, "60000"));
	CHECK(hash32_32.batch_query(80000, "80000"));
	CHECK(hash32_32.batch_query(100000, "100000"));
	hash32_32.clear_env();

	auto hash32_64 = hash_batch_test<32, 64>(fd, tbl_id, tbl_deepth);
	hash32_64.init_env(true);
	CHECK(hash32_64.batch_query(100, "100"));
	CHECK(hash32_64.batch_query(1000, "1000"));
	CHECK(hash32_64.batch_query(10000, "10000"));
	CHECK(hash32_64.batch_query(20000, "20000"));
	CHECK(hash32_64.batch_query(40000, "40000"));
	CHECK(hash32_64.batch_query(60000, "60000"));
	CHECK(hash32_64.batch_query(80000, "80000"));
	CHECK(hash32_64.batch_query(100000, "100000"));
	hash32_64.clear_env();

	auto hash32_128 = hash_batch_test<32, 128>(fd, tbl_id, tbl_deepth);
	hash32_128.init_env(true);
	CHECK(hash32_128.batch_query(100, "100"));
	CHECK(hash32_128.batch_query(1000, "1000"));
	CHECK(hash32_128.batch_query(10000, "10000"));
	CHECK(hash32_128.batch_query(20000, "20000"));
	CHECK(hash32_128.batch_query(40000, "40000"));
	CHECK(hash32_128.batch_query(60000, "60000"));
	CHECK(hash32_128.batch_query(80000, "80000"));
	CHECK(hash32_128.batch_query(100000, "100000"));
	hash32_32.clear_env();

	hados_doe_release_fd(fd);
}

void hash_batch_delete_test()
{
	int fd = hados_doe_request_fd();
	if (fd == -1) {
		std::cout << "call yusdoe_request_fd failed." << std::endl;
		return;
	}

	uint8_t tbl_id = 1;
	uint32_t tbl_deepth = 10000000;

	auto hash32_32 = hash_batch_test<32, 32>(fd, tbl_id, tbl_deepth);
	CHECK(hash32_32.batch_del(100, "100"));
	CHECK(hash32_32.batch_del(1000, "1000"));
	CHECK(hash32_32.batch_del(10000, "10000"));
	CHECK(hash32_32.batch_del(20000, "20000"));
	CHECK(hash32_32.batch_del(40000, "40000"));
	CHECK(hash32_32.batch_del(60000, "60000"));
	CHECK(hash32_32.batch_del(80000, "80000"));

	auto hash32_64 = hash_batch_test<32, 64>(fd, tbl_id, tbl_deepth);
	CHECK(hash32_64.batch_del(100, "100"));
	CHECK(hash32_64.batch_del(1000, "1000"));
	CHECK(hash32_64.batch_del(10000, "10000"));
	CHECK(hash32_64.batch_del(20000, "20000"));
	CHECK(hash32_64.batch_del(40000, "40000"));
	CHECK(hash32_64.batch_del(60000, "60000"));
	CHECK(hash32_64.batch_del(80000, "80000"));

	auto hash32_128 = hash_batch_test<32, 128>(fd, tbl_id, tbl_deepth);
	CHECK(hash32_128.batch_del(100, "100"));
	CHECK(hash32_128.batch_del(1000, "1000"));
	CHECK(hash32_128.batch_del(10000, "10000"));
	CHECK(hash32_128.batch_del(20000, "20000"));
	CHECK(hash32_128.batch_del(40000, "40000"));
	CHECK(hash32_128.batch_del(60000, "60000"));
	CHECK(hash32_128.batch_del(80000, "80000"));

	hados_doe_release_fd(fd);
}

typedef void (*func)();

static std::unordered_map<std::string, func> g_functions = {
	{"store",		&array_store_test},
	{"load",		&array_load_test},
	{"readclear",		&array_readclear_test},
	{"batch_store",		&array_batch_store_test},
	{"batch_load",		&array_batch_load_test},
	{"batch_readclear",	&array_batch_readclear_test},
	{"insert",		&hash_insert_test},
	{"query",		&hash_query_test},
	{"delete",		&hash_delete_test},
	{"batch_insert",	&hash_batch_insert_test},
	{"batch_query",		&hash_batch_query_test},
	{"batch_delete",	&hash_batch_delete_test},
};

TEST_CASE("TEST")
{
	auto it = g_functions.find(argu.test_doc);
	if (it == g_functions.end())
		return;

	(*it).second();
}

int main(int argc, char *argv[])
{
	argp_parse(&argp, argc, argv, 0, 0, &argu);
	if (argu.test_doc.empty() ||
		g_functions.find(argu.test_doc) == g_functions.end()) {
		printf("invalid command! %s\n", argu.test_doc.c_str());
		return -1;
	}

	doctest::Context ctx;
	ctx.setOption("abort-after", 5);	// default - stop after 5 failed asserts
	ctx.applyCommandLine(argc, argv);	// apply command line - argc / argv
	ctx.setOption("no-breaks", true);	// override - don't break in the debugger
	int res = ctx.run();			// run test cases unless with --no-run
	if(ctx.shouldExit())			// query flags (and --exit) rely on this
		return res;                   	// propagate the result of the tests
	// your actual program execution goes here - only if we haven't exited
	return res; // + your_program_res
}