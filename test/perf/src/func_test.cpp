/**
 * @Author: Xing Wang (wangx_ddpt@yusur.tech)
 * @date: 2023-03-25 22:41:48
 * @last_author: Xing Wang (wangx_ddpt@yusur.tech)
 * @last_edit_time: 2023-04-23 18:13:03
 */

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

extern "C" {
	#include "hados_doe.h"
}

static char value[] = "hello,world";
static uint32_t per_count = 10000;

template <uint8_t DATA_LEN>
class doe_array
{
	struct item_t {
		uint32_t    index;
		char        data[DATA_LEN];
	};

	struct index_t {
		int index;
	};

public:
	doe_array(int fd, uint8_t tbl_id, uint32_t depth)
		: _fd(fd), _tbl_id(tbl_id), _depth(depth)
	{
		int ret = hados_doe_create_arraytbl(_fd, _tbl_id, _depth, DATA_LEN, NULL);
		CHECK_MESSAGE(ret == DOE_RESULT_SUCCESS, "创建%d字节数组表失败", DATA_LEN);

		/* malloc data for batch insert */
		_insert_data = (item_t *)malloc(sizeof(item_t) * _depth);
		memset(_insert_data, 0, sizeof(item_t) * _depth);

		/* malloc index for batch load */
		_indexs = (uint32_t*)malloc(sizeof(uint32_t) * _depth);

		/* init data and index */
		for (uint32_t i = 0; i < _depth; ++i) {
			_indexs[i] = i;
			_insert_data[i].index = i;
			memcpy(_insert_data[i].data, value, sizeof(value));
		}
	}

	~doe_array()
	{
		free(_insert_data);
		free(_indexs);
		hados_doe_delete_arraytbl(_fd, _tbl_id);
	}

	bool store_all()
	{
		int ret;

		for(uint32_t i = 0; i < _depth/per_count; ++i) {
			ret = hados_doe_array_store_batch(_fd, _tbl_id, per_count, _insert_data + i * per_count);
			REQUIRE_MESSAGE(ret == per_count, "插入数据失败。index=%d", i);
		}

		return ret;
	}

	void check_all()
	{
		bool flag;

		auto tmp = std::make_unique<item_t[]>(per_count);
		for(uint32_t i = 0; i < _depth/per_count; ++i) {
			int ret = hados_doe_array_load_batch(_fd, _tbl_id, per_count, _indexs + i * per_count, tmp.get());
			REQUIRE_MESSAGE(ret == per_count, "读取数据失败。index=%d", i);

			for (uint32_t j = 0; j < per_count; ++j) {
				flag = memcmp(_insert_data[tmp[j].index].data, value, sizeof(value)) == 0;
				REQUIRE_MESSAGE(flag, "校验数据失败。index=%d", j);
			}
		}
	}

	void delete_and_check()
	{
		bool flag;

		auto tmp = std::make_unique<item_t[]>(per_count);
		for(uint32_t i = 0; i < _depth/per_count; ++i) {
			int ret = hados_doe_array_readclear_batch(_fd, _tbl_id, per_count, _indexs + i * per_count, tmp.get());
			REQUIRE_MESSAGE(ret == per_count, "读清数据失败。index=%d", i);

			for (uint32_t j = 0; j < per_count; ++j) {
				flag = memcmp(_insert_data[tmp[j].index].data, value, sizeof(value)) == 0;
				REQUIRE_MESSAGE(flag, "校验数据失败。index=%d", j);
			}

			ret = hados_doe_array_load_batch(_fd, _tbl_id, per_count, _indexs + i * per_count, tmp.get());
			for (uint32_t j = 0; j < per_count; ++j) {
				REQUIRE_MESSAGE(strlen(tmp[j].data) == 0, "校验数据失败。index=%d", j);
			}
		}
	}

private:
	item_t*     _insert_data;
	uint32_t*   _indexs;
	int         _fd;
	uint8_t     _tbl_id;
	uint32_t    _depth;
};

template <uint8_t KEY_LEN, uint8_t VAL_LEN>
class doe_hash
{
	struct item_t
	{
		char key[KEY_LEN];
		char val[VAL_LEN];
	} __attribute__((packed));

	struct key_t {
		char key[KEY_LEN];
	} __attribute__((packed));

private:
	item_t*     _data;
	char*      _keys;
	int         _fd;
	uint8_t     _tbl_id;
	uint32_t    _depth;

public:
	doe_hash(int fd, uint8_t tbl_id, uint32_t depth)
	: _fd(fd), _tbl_id(tbl_id), _depth(depth)
	{
		int ret = hados_doe_create_hashtbl(_fd, _tbl_id, _depth, KEY_LEN, VAL_LEN, NULL);
		CHECK_MESSAGE(ret == DOE_RESULT_SUCCESS, "创建(%d-%d)哈希表失败", KEY_LEN, VAL_LEN);

		_data = new item_t[_depth];
		memset(_data, 0, sizeof(item_t) * _depth);

		_keys = (char *)malloc(KEY_LEN * _depth);
		memset(_keys, 0, KEY_LEN * _depth);

		for (uint32_t i = 0; i < _depth; ++i) {
			std::string tmp("hello");
			tmp += std::to_string(i);

			memcpy(_data[i].key, tmp.c_str(), tmp.length());
			memcpy(_keys + KEY_LEN * i, tmp.c_str(), tmp.length());
			memcpy(_data[i].val, value, sizeof(value));
		}
	}

	~doe_hash()
	{
		delete[] _data;
		free(_keys);
		hados_doe_delete_hashtbl(_fd, _tbl_id);
	}

	bool insert_all()
	{
		int ret;

		for (uint32_t i = 0; i < _depth/per_count; ++i) {
			ret = hados_doe_hash_insert_batch(_fd, _tbl_id, per_count, _data + i * per_count);
			REQUIRE_MESSAGE(ret == per_count, "插入数据失败。index=%d", i);
		}

		return true;
	}

	bool query_all()
	{
		auto tmp = std::make_unique<item_t[]>(per_count);
		for(uint32_t i = 0; i < _depth/per_count; ++i) {
			int ret = hados_doe_hash_query_batch(_fd, _tbl_id, per_count, _keys + i * per_count * KEY_LEN, tmp.get());
			REQUIRE_MESSAGE(ret == per_count, "读取数据失败。index=%d", i);

			for (uint32_t j = 0; j < per_count; ++j) {
				REQUIRE_MESSAGE(memcmp(tmp[j].val, value, sizeof(value)) == 0, "校验数据失败。index=%d", j);
			}
		}

		return true;
	}

	bool del_and_check()
	{
		auto tmp = std::make_unique<item_t[]>(per_count);
		for(uint32_t i = 0; i < _depth/per_count; ++i) {
			int ret = hados_doe_hash_delete_batch(_fd, _tbl_id, per_count, _keys + i * per_count * KEY_LEN);
			REQUIRE_MESSAGE(ret == per_count, "删除数据失败。index=%d", i);

			ret = hados_doe_hash_query_batch(_fd, _tbl_id, per_count, _keys + i * per_count * KEY_LEN, tmp.get());
			REQUIRE_MESSAGE(ret == 0, "删除数据后读取数据成功。index=%d", i);
		}

		return true;
	}
};

uint8_t tbl1_id = 1;
uint8_t tbl2_id = 11;
uint8_t tbl3_id = 111;

TEST_CASE("创建数组表")
{
	int fd = hados_doe_request_fd();
	REQUIRE_MESSAGE(fd != -1, "打开DOE文件描述符失败");

	SUBCASE("创建32B数组表")
	{
		auto array_32 = doe_array<32>(fd, tbl1_id, 8000000);
	}

	SUBCASE("创建64B数组表")
	{
		auto array_64 = doe_array<64>(fd, tbl2_id, 8000000);
	}

	SUBCASE("创建128B数组表")
	{
		auto array_128 = doe_array<128>(fd, tbl3_id, 2000000);
	}

	hados_doe_release_fd(fd);
}

TEST_CASE("数据表store指令")
{
	int fd = hados_doe_request_fd();
	REQUIRE_MESSAGE(fd != -1, "打开DOE文件描述符失败");

	SUBCASE("32B数据表存储")
	{
		auto array_32 = doe_array<32>(fd, tbl1_id, 8000000);
		array_32.store_all();
	}

	SUBCASE("64B数据表存储")
	{
		auto array_64 = doe_array<64>(fd, tbl2_id, 8000000);
		array_64.store_all();
	}

	SUBCASE("128B数据表存储")
	{
		auto array_128 = doe_array<128>(fd, tbl3_id, 2000000);
		array_128.store_all();
	}

	hados_doe_release_fd(fd);
}

TEST_CASE("数组表load指令")
{
	int fd = hados_doe_request_fd();
	REQUIRE_MESSAGE(fd != -1, "打开DOE文件描述符失败");

	SUBCASE("32B数据表存储")
	{
		auto array_32 = doe_array<32>(fd, tbl1_id, 8000000);
		array_32.store_all();
		array_32.check_all();
	}

	SUBCASE("64B数据表存储")
	{
		auto array_64 = doe_array<64>(fd, tbl2_id, 8000000);
		array_64.store_all();
		array_64.check_all();
	}

	SUBCASE("128B数据表存储")
	{
		auto array_128 = doe_array<128>(fd, tbl3_id, 2000000);
		array_128.store_all();
		array_128.check_all();
	}

	hados_doe_release_fd(fd);
}

TEST_CASE("数据表readclear指令")
{
	int fd = hados_doe_request_fd();
	REQUIRE_MESSAGE(fd != -1, "打开DOE文件描述符失败");

	SUBCASE("32B数据表读清")
	{
		auto array_32 = doe_array<32>(fd, tbl1_id, 8000000);
		array_32.store_all();
		array_32.delete_and_check();
	}

	SUBCASE("64B数据表读清")
	{
		auto array_64 = doe_array<64>(fd, tbl2_id, 8000000);
		array_64.store_all();
		array_64.delete_and_check();
	}

	SUBCASE("128B数据表读清")
	{
		auto array_128 = doe_array<128>(fd, tbl3_id, 2000000);
		array_128.store_all();
		array_128.delete_and_check();
	}

	hados_doe_release_fd(fd);
}

TEST_CASE("哈希表创建")
{
	int fd = hados_doe_request_fd();
	REQUIRE_MESSAGE(fd != -1, "打开DOE文件描述符失败");

	SUBCASE("创建32B-32B哈希表")
	{
		auto hash32_32 = doe_hash<32,32>(fd, tbl1_id, 8000000);
	}

	SUBCASE("创建64B-64B哈希表")
	{
		auto hash64_64 = doe_hash<64, 64>(fd, tbl2_id, 4000000);
	}

	SUBCASE("创建128B-128B哈希表")
	{
		auto hash128_128 = doe_hash<128, 128>(fd, tbl3_id, 2000000);
	}

	hados_doe_release_fd(fd);
}

TEST_CASE("哈希表insert指令")
{
	int fd = hados_doe_request_fd();
	REQUIRE_MESSAGE(fd != -1, "打开DOE文件描述符失败");

	SUBCASE("32B-32B哈希表插入")
	{
		auto hash32_32 = doe_hash<32,32>(fd, tbl1_id, 8000000);
		hash32_32.insert_all();
	}

	SUBCASE("64B-64B哈希表插入")
	{
		auto hash64_64 = doe_hash<64, 64>(fd, tbl2_id, 4000000);
		hash64_64.insert_all();
	}

	SUBCASE("128B-128B哈希表插入")
	{
		auto hash128_128 = doe_hash<128, 128>(fd, tbl3_id, 2000000);
		hash128_128.insert_all();
	}

	hados_doe_release_fd(fd);
}

TEST_CASE("哈希表query指令")
{
	int fd = hados_doe_request_fd();
	REQUIRE_MESSAGE(fd != -1, "打开DOE文件描述符失败");

	SUBCASE("32B-32B哈希表查询")
	{
		auto hash32_32 = doe_hash<32,32>(fd, tbl1_id, 8000000);
		hash32_32.insert_all();
		hash32_32.query_all();
	}

	SUBCASE("64B-64B哈希表查询")
	{
		auto hash64_64 = doe_hash<64, 64>(fd, tbl2_id, 4000000);
		hash64_64.insert_all();
		hash64_64.query_all();
	}

	SUBCASE("128B-128B哈希表查询")
	{
		auto hash128_128 = doe_hash<128, 128>(fd, tbl3_id, 2000000);
		hash128_128.insert_all();
		hash128_128.query_all();
	}

	hados_doe_release_fd(fd);
}

TEST_CASE("哈希表delete指令")
{
	int fd = hados_doe_request_fd();
	REQUIRE_MESSAGE(fd != -1, "打开DOE文件描述符失败");

	SUBCASE("32B-32B哈希表删除")
	{
		auto hash32_32 = doe_hash<32,32>(fd, tbl1_id, 8000000);
		hash32_32.insert_all();
		hash32_32.del_and_check();
	}

	SUBCASE("64B-64B哈希表删除")
	{
		auto hash64_64 = doe_hash<64, 64>(fd, tbl2_id, 4000000);
		hash64_64.insert_all();
		hash64_64.del_and_check();
	}

	SUBCASE("128B-128B哈希表删除")
	{
		auto hash128_128 = doe_hash<128, 128>(fd, tbl3_id, 2000000);
		hash128_128.insert_all();
		hash128_128.del_and_check();
	}

	hados_doe_release_fd(fd);
}




