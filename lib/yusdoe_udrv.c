#include <argp.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "hados_doe.h"
#include "../modules/include/uapi_yusdoe.h"

int hados_doe_request_fd(void)
{
	return open("/dev/yusdoe-0", O_RDONLY, 0);
}

void hados_doe_release_fd(int fd)
{
	close(fd);
}

static doe_result_t send_cmd(int fd, struct yusdoe_sw_cmd *cmd)
{
	int ret;

	ret = ioctl(fd, YUSDOE_SEND_CMD, cmd);
	if (ret) {
		if (errno == ETIMEDOUT)
			return DOE_RESULT_TIMEOUT;
		else if (errno == ENOMEM)
			return DOE_RESULT_NOMEM;
		else if (errno == EINVAL)
			return DOE_RESULT_TBL_ERR;
	} 
	
	if (cmd->cnt != cmd->succeed)
		return HARDWARE_ERR_BEGIN + cmd->err;

	return DOE_RESULT_SUCCESS;
}

/* If user table is not the power of 2, find next value */
static uint32_t find_next_power_of_2(uint32_t depth)
{
	uint32_t target = 2;

	while (target < depth)
		target <<= 1;

	return target;
}

doe_result_t hados_doe_create_arraytbl(int fd, uint8_t table_id, uint32_t depth,
				    uint8_t data_len,
				    const struct hw_advance_cfg *cfg)
{
	struct yusdoe_sw_cmd cmd;

	memset(&cmd, 0, sizeof(cmd));
	cmd.opcode = YUSDOE_SW_CREATE_ARRAY;
	cmd.tbl_id = table_id;
	cmd.tbl_param.index_mask = find_next_power_of_2(depth) - 1;
	cmd.tbl_param.dov_len = data_len;
	cmd.tbl_param.index_sram_size = 512;
	cmd.tbl_param.cache_ways = 1;
	if (cfg) {
		cmd.tbl_param.use_cache = true;
		cmd.tbl_param.shared_tbl = cfg->shared_tbl;
		memcpy(cmd.tbl_param.l1_cache_ways, cfg->l1_cache_ways, sizeof(uint8_t) * 16);
		cmd.tbl_param.l2_cache_ways = cfg->l2_cache_ways;
	} else {
		cmd.tbl_param.use_cache = false;
	}

	return send_cmd(fd, &cmd);
}

doe_result_t hados_doe_create_hashtbl(int fd, uint8_t table_id, uint32_t depth,
				   uint8_t key_len, uint8_t value_len,
				   const struct hw_advance_cfg *cfg)
{
	struct yusdoe_sw_cmd cmd;

	memset(&cmd, 0, sizeof(cmd));
	cmd.opcode = YUSDOE_SW_CREATE_HASH;
	cmd.tbl_id = table_id;
	cmd.tbl_param.index_mask = find_next_power_of_2(depth) - 1;
	cmd.tbl_param.key_len = key_len;
	cmd.tbl_param.dov_len = value_len;
	if (cmd.tbl_param.index_mask > 256)
		cmd.tbl_param.index_sram_size = 256;
	else
		cmd.tbl_param.index_sram_size = depth;

	cmd.tbl_param.cache_ways = 1;
	if (cfg) {
		cmd.tbl_param.use_cache = true;
		cmd.tbl_param.shared_tbl = cfg->shared_tbl;
		memcpy(cmd.tbl_param.l1_cache_ways, cfg->l1_cache_ways, sizeof(uint8_t) * 16);
		cmd.tbl_param.l2_cache_ways = cfg->l2_cache_ways;
	} else {
		cmd.tbl_param.use_cache = false;
	}

	return send_cmd(fd, &cmd);
}

doe_result_t hados_doe_delete_arraytbl(int fd, uint8_t table_id)
{
	struct yusdoe_sw_cmd cmd;

	memset(&cmd, 0, sizeof(cmd));
	cmd.opcode = YUSDOE_SW_DELETE_ARRAY;
	cmd.tbl_id = table_id;

	return send_cmd(fd, &cmd);
}

doe_result_t hados_doe_delete_hashtbl(int fd, uint8_t table_id)
{
	struct yusdoe_sw_cmd cmd;

	memset(&cmd, 0, sizeof(cmd));
	cmd.opcode = YUSDOE_SW_DELETE_HASH;
	cmd.tbl_id = table_id;

	return send_cmd(fd, &cmd);
}

/* array table single operation */
doe_result_t hados_doe_array_store(int fd, uint8_t table_id, uint32_t index,
				const void *data, uint8_t size)
{
	struct yusdoe_sw_cmd cmd;

	memset(&cmd, 0, sizeof(cmd));
	cmd.opcode = YUSDOE_SW_ARRAY_STORE;
	cmd.tbl_id = table_id;
	cmd.index = index;
	memcpy(cmd.data, data, size > 128 ? 128 : size);

	return send_cmd(fd, &cmd);
}

doe_result_t hados_doe_array_load(int fd, uint8_t table_id, uint32_t index,
			       void *data, uint8_t size)
{
	struct yusdoe_sw_cmd cmd;
	doe_result_t ret;

	memset(&cmd, 0, sizeof(cmd));
	cmd.opcode = YUSDOE_SW_ARRAY_LOAD;
	cmd.tbl_id = table_id;
	cmd.index = index;
	cmd.is_read = 1;

	ret = send_cmd(fd, &cmd);
	if (ret != DOE_RESULT_SUCCESS)
		return ret;

	memcpy(data, cmd.data, size > 128 ? 128 : size);

	return DOE_RESULT_SUCCESS;
}

doe_result_t hados_doe_array_read_clear(int fd, uint8_t table_id, uint32_t index,
				     void *data, uint8_t size)
{
	struct yusdoe_sw_cmd cmd;
	doe_result_t ret;

	memset(&cmd, 0, sizeof(cmd));
	cmd.opcode = YUSDOE_SW_ARRAY_READ_CLEAR;
	cmd.tbl_id = table_id;
	cmd.index = index;
	cmd.is_read = 1;

	ret = send_cmd(fd, &cmd);
	if (ret != DOE_RESULT_SUCCESS)
		return ret;

	memcpy(data, cmd.data, size > 128 ? 128 : size);

	return DOE_RESULT_SUCCESS;
}

/* hash table single operation */
doe_result_t hados_doe_hash_insert(int fd, uint8_t table_id, const void *key,
				uint8_t key_len, const void *value,
				uint8_t value_len)
{
	struct yusdoe_sw_cmd cmd;

	memset(&cmd, 0, sizeof(cmd));
	cmd.opcode = YUSDOE_SW_HASH_INSERT;
	cmd.tbl_id = table_id;
	memcpy(cmd.key, key, key_len > 128 ? 128 : key_len);
	memcpy(cmd.value, value, value_len > 128 ? 128 : value_len);

	return send_cmd(fd, &cmd);
}

doe_result_t hados_doe_hash_query(int fd, uint8_t table_id, const void *key,
			       uint8_t key_len, void *value, uint8_t value_len)
{
	struct yusdoe_sw_cmd cmd;
	doe_result_t ret;

	memset(&cmd, 0, sizeof(cmd));
	cmd.opcode = YUSDOE_SW_HASH_QUERY;
	cmd.tbl_id = table_id;
	cmd.is_read = 1;
	memcpy(cmd.key, key, key_len > 128 ? 128 : key_len);

	ret = send_cmd(fd, &cmd);
	if (ret != DOE_RESULT_SUCCESS)
		return ret;

	memcpy(value, cmd.value, value_len > 128 ? 128 : value_len);

	return DOE_RESULT_SUCCESS;
}

doe_result_t hados_doe_hash_delete(int fd, uint8_t table_id, const void *key,
				uint8_t key_len)
{
	struct yusdoe_sw_cmd cmd;

	memset(&cmd, 0, sizeof(cmd));
	cmd.opcode = YUSDOE_SW_HASH_DELETE;
	cmd.tbl_id = table_id;
	memcpy(cmd.key, key, key_len > 128 ? 128 : key_len);

	return send_cmd(fd, &cmd);
}

static inline uint32_t send_batch_cmd(int fd, enum yusur_doe_cmd_opcode opc,
				      uint8_t table_id, int is_read,
				      uint32_t cnt,
				      void *koi_list, void *pair_list)
{
	struct yusdoe_sw_cmd cmd;

	memset(&cmd, 0, sizeof(cmd));
	cmd.opcode = opc;
	cmd.tbl_id = table_id;
	cmd.cnt = cnt;
	cmd.koi_list = koi_list;
	cmd.pair_list = pair_list;
	cmd.is_read = is_read;

	send_cmd(fd, &cmd);

	return cmd.succeed;
}

uint32_t hados_doe_array_store_batch(int fd, uint8_t table_id, uint32_t cnt,
				  const void *pair_list)
{
	return send_batch_cmd(fd, YUSDOE_SW_ARRAY_STORE_BATCH, table_id, 0, cnt,
			      NULL, (void *)pair_list);
}

uint32_t hados_doe_array_load_batch(int fd, uint8_t table_id, uint32_t cnt,
				 const void *index_list, void *pair_list)
{
	return send_batch_cmd(fd, YUSDOE_SW_ARRAY_LOAD_BATCH, table_id, 1, cnt,
			      (void *)index_list, pair_list);
}

uint32_t hados_doe_array_readclear_batch(int fd, uint8_t table_id, uint32_t cnt,
				      const void *index_list, void *pair_list)
{
	return send_batch_cmd(fd, YUSDOE_SW_ARRAY_READ_CLEAR_BATCH, table_id, 1,
			      cnt, (void *)index_list, pair_list);
}

uint32_t hados_doe_hash_insert_batch(int fd, uint8_t table_id, uint32_t cnt,
				  const void *pair_list)
{
	return send_batch_cmd(fd, YUSDOE_SW_HASH_INSERT_BATCH, table_id, 0, cnt,
			      NULL, (void *)pair_list);
}

uint32_t hados_doe_hash_query_batch(int fd, uint8_t table_id, uint32_t cnt,
				 const void *key_list, void *pair_list)
{
	return send_batch_cmd(fd, YUSDOE_SW_HASH_QUERY_BATCH, table_id, 1, cnt,
			      (void *)key_list, pair_list);
}

uint32_t hados_doe_hash_delete_batch(int fd, uint8_t table_id, uint32_t cnt,
				  const void *key_list)
{
	return send_batch_cmd(fd, YUSDOE_SW_HASH_DELETE_BATCH, table_id, 0, cnt,
			      (void *)key_list, NULL);
}
