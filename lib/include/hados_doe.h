/**
 * @file hados_doe.h
 * @brief hasdos_doe header file
 * @version 1.3
 * @copyright Copyright (c) 2020 YUSUR Technology Co., Ltd. All Rights Reserved. Learn more at www.yusur.tech.
 * @author Xing Wang (wangx_ddpt@yusur.tech)
 * @date 2022-11-29 10:25:59
 * @last_author: Xing Wang (wangx_ddpt@yusur.tech)
 * @last_edit_time: 2023-11-07 16:25:11
 * @defgroup DOE DOE模块
 * @{
 */

#ifndef __HADOS_DOE_H_
#define __HADOS_DOE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>

#define HARDWARE_ERR_BEGIN 100

/**
 * @brief 硬件相关参数
 * @details l1缓存被分为16个通道，每个通道包含32个tag
 *		l2缓存没有划分通道，每次最大配置128
 */
struct hw_advance_cfg {
	bool shared_tbl;			///< 是否共享表
	uint8_t l1_cache_ways[16];		///< 16个l1缓存通道上分别占用多少way
	uint8_t l2_cache_ways;			///< 占用l2缓存多少个way
};

/**
 * @brief 操作返回值
 */
typedef enum {
	DOE_RESULT_SUCCESS,			///< 成功
	DOE_RESULT_TIMEOUT,			///< 超时
	DOE_RESULT_NOMEM,			///< 内存不足
	DOE_RESULT_TBL_ERR,			///< 表错误
	DOE_RESULT_IDX_ERR,			///< 索引错误
	DOE_RESULT_DATA_ERR,			///< 数据错误
	DOE_RESULT_HASH_INSET_ERR,		///< 哈希表插入错误
	/* create table err */
	DOE_RESULT_CACHE_TAG_ERR,		///< 缓存tag错误
	DOE_RESULT_CACHE_DATA_ERR,		///< 缓存数据错误
	DOE_RESULT_INDEX_SRAM_ERR,		///< SRM的索引错误
	DOE_RESULT_INDEX_SOURCE_ERR,		///< 索引源错误
	/* hardware err */
	DOE_RESULT_HW_TBL_INVALID = HARDWARE_ERR_BEGIN + 3,	///< 无效表
	DOE_RESULT_HW_IDX_INVALID = HARDWARE_ERR_BEGIN + 5,	///< 无效索引
	DOE_RESULT_HW_DDR_ERR	  = HARDWARE_ERR_BEGIN + 7,	///< DDR写异常
	DOE_RESULT_HW_KEY_INVALID = HARDWARE_ERR_BEGIN + 9,	///< 无效的KEY
	DOE_RESULT_HW_CMD_LEN_ERR = HARDWARE_ERR_BEGIN + 17,	///< 指令长度错误
} doe_result_t;

/**
 * @brief 创建doe句柄
 *
 * @return 成功返回doe句柄，失败返回-1
 */
int hados_doe_request_fd(void);

/**
 * @brief 关闭doe句柄
 *
 * @param [in] fd doe句柄。由接口hados_doe_request_fd创建
 */
void hados_doe_release_fd(int fd);


/**
 * @brief 创建数组表
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @param [in] depth		表最大容量
 * @param [in] data_len		表数据长度
 * @param [in] cfg			硬件配置。现阶段传NULL
 * @return doe_result_t
 */
doe_result_t hados_doe_create_arraytbl(int fd, uint8_t table_id, uint32_t depth,
				    uint8_t data_len,
				    const struct hw_advance_cfg *cfg);

/**
 * @brief 创建哈希表
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @param [in] depth		表最大容量
 * @param [in] key_len		索引值最大长度
 * @param [in] value_len	索引数据最大长度
 * @param [in] cfg			硬件配置。现阶段传NULL
 * @return doe_result_t
 */
doe_result_t hados_doe_create_hashtbl(int fd, uint8_t table_id, uint32_t depth,
				   uint8_t key_len, uint8_t value_len,
				   const struct hw_advance_cfg *cfg);

/**
 * @brief 删除数组表
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @return doe_result_t
 */
doe_result_t hados_doe_delete_arraytbl(int fd, uint8_t table_id);

/**
 * @brief 删除哈希表
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @return doe_result_t
 */
doe_result_t hados_doe_delete_hashtbl(int fd, uint8_t table_id);

/**
 * @brief 数组表存储
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @param [in] index		数组表索引
 * @param [in] data			数据指针
 * @param [in] size			数据长度
 * @return doe_result_t
 */
doe_result_t hados_doe_array_store(int fd, uint8_t table_id, uint32_t index,
				const void *data, uint8_t size);

/**
 * @brief 数组表读取
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @param [in] index		数组表索引
 * @param [out] data		数据指针，必须指向有效的内存空间
 * @param [in] size			数据长度
 * @return doe_result_t
 */
doe_result_t hados_doe_array_load(int fd, uint8_t table_id, uint32_t index,
			       void *data, uint8_t size);

/**
 * @brief 数组表读取清除数据
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @param [in] index		数组表索引
 * @param [out] data		数据指针，必须指向有效的内存空间
 * @param [in] size			数据长度
 * @return doe_result_t
 */
doe_result_t hados_doe_array_read_clear(int fd, uint8_t table_id, uint32_t index,
				     void *data, uint8_t size);

/**
 * @brief 哈希表插入
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @param [in] key			key值
 * @param [in] key_len		key长度
 * @param [in] value		value值
 * @param [in] value_len	value长度
 * @return doe_result_t
 */
doe_result_t hados_doe_hash_insert(int fd, uint8_t table_id, const void *key,
				uint8_t key_len, const void *value,
				uint8_t value_len);

/**
 * @brief 哈希表查询
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @param [in] key			key值
 * @param [in] key_len		key长度
 * @param [out] value		value值
 * @param [in] value_len	value长度
 * @return doe_result_t
 */
doe_result_t hados_doe_hash_query(int fd, uint8_t table_id, const void *key,
			       uint8_t key_len, void *value, uint8_t value_len);

/**
 * @brief 哈希表删除
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @param [in] key			key值
 * @param [in] key_len		key长度
 * @return doe_result_t
 */
doe_result_t hados_doe_hash_delete(int fd, uint8_t table_id, const void *key,
				uint8_t key_len);

/**
 * @brief 数组表批量存储
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @param [in] cnt			数据个数
 * @param [in] pair_list	{index : data}数据组
 * @return 批量操作成功次数
 */
uint32_t hados_doe_array_store_batch(int fd, uint8_t table_id, uint32_t cnt,
				  const void *pair_list);

/**
 * @brief 数组表批量读取
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @param [in] cnt			数据个数
 * @param [in] index_list	索引列表
 * @param [out] pair_list	{index:data}数据组
 * @return 批量操作成功次数
 */
uint32_t hados_doe_array_load_batch(int fd, uint8_t table_id, uint32_t cnt,
				 const void *index_list, void *pair_list);

/**
 * @brief 数组表批量读取清除
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @param [in] cnt			数据个数
 * @param [in] index_list	索引列表
 * @param [out] pair_list	{index:data}数据组
 * @return 批量操作成功次数
 */
uint32_t hados_doe_array_readclear_batch(int fd, uint8_t table_id, uint32_t cnt,
				      const void *index_list, void *pair_list);

/**
 * @brief 哈希表批量插入
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @param [in] cnt			数据个数
 * @param [in] pair_list	{key:value} 数据组
 * @return 批量操作成功次数
 */
uint32_t hados_doe_hash_insert_batch(int fd, uint8_t table_id, uint32_t cnt,
				  const void *pair_list);

/**
 * @brief 哈希表批量查询
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @param [in] cnt			数据个数
 * @param [in] key_list		key值列表
 * @param [out] pair_list	{key:value}数据组
 * @return 批量操作成功次数
 */
uint32_t hados_doe_hash_query_batch(int fd, uint8_t table_id, uint32_t cnt,
				 const void *key_list, void *pair_list);

/**
 * @brief 哈希表批量删除
 *
 * @param [in] fd			doe句柄
 * @param [in] table_id		表号
 * @param [in] cnt			数据个数
 * @param [in] key_list		key值列表
 * @return 批量操作成功次数
 */
uint32_t hados_doe_hash_delete_batch(int fd, uint8_t table_id, uint32_t cnt,
				  const void *key_list);

#ifdef __cplusplus
}
#endif

#endif /* __HADOS_DOE_H_*/

/** @}*/
