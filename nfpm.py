#!/usr/bin/env python
# coding=utf-8

import os
import os.path
import yaml
import argparse

repository = ''

#读取yaml文件
pwd_path = os.path.abspath(".")
file_path = os.path.join(pwd_path, "hados-ci.yaml")

#解析yaml文件
with open(file_path, mode="r", encoding="utf-8") as f:
    if repository == "CentOS-Stream-8":
        yamlConf = yaml.load(f.read())
    else:
        yamlConf = yaml.load(f.read(), Loader=yaml.FullLoader)

fileslist = yamlConf["package"][0]["contents"]

for files in fileslist:
  print(files["src"])
  print(files["dst"])

print(fileslist)
