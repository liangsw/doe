@page manual-DOE HADOS-DOE
@tableofcontents

# 模块简介
## 模块简介
`DOE`(数据库卸载引擎)全称是`Database Offload Engine`，是中科驭数针对高性能数据处理设计的硬件模块。`DOE`主要原理是将数据处理过程卸载到专用硬件上，由专用硬件对数据进行收集、组织、存储、加工，以达到高速甚至实时处理的能力。传统数据库由CPU、内存、硬盘等硬件来支撑其能力，一般由软件实现。这些硬件资源会由很多应用共用，会导致资源受限而存在性能瓶颈。`DOE`将数据处理卸载到硬件，能充分利用硬件能力，而且硬件处理速度比软件快，能极大提高数据处理速度。

## 模块架构
`DOE`由三部分组成。`SDK`、驱动以及硬件。具体架构如下图所示：

![DOE架构图](image/536103116248579.png)

`SDK`：对用户应用提供标准接口，实现对`DOE`模块访问控制。包含头文件`hados_doe.h`和动态库`libhadosdoe.so`
驱动：运行在系统内核中。`SDK`与硬件中间层。包含`hadosdoe.ko`文件
硬件：运行在硬件板卡中。通过`PCIe`总线与驱动交互

## 应用场景
`DOE`支持数组表和哈希表两种数据结构。数组表是连续的数据结构，通过索引进行访问。哈希表是`key-value`结构，通过`key`来进行访问。
高速处理这两种数据是`DOE`的典型应用场景。
### 风控一期
项目中需要操作哈希表，通过软件处理达不到性能要求。将表操作放到硬件上通过`DOE`进行加速后，能够满足性能要求。

### K2-Pro
在`K2-Pro`中，`OVS`需要非常频繁对流表进行增删改查，因此对性能要求非常高。这正是`DOE`擅长领域及。`DOE`主要作用是流表数据操作。

# 安装配置
## 安装指导
`DOE`提供的是`RPM`安装包。执行以下命令进行安装。

> $ sudo rpm -i hados_doe-1.0-1.1.x86_64.rpm

成功后`DOE`开发依赖包会被安装到`/opt/yusur/hados`目录。

## 运行指导
无

## 配置指导
### 安装驱动
驱动在`/opt/yusur/hados/modules`目录。执行下面命令进行驱动安装。
> insmod hados_doe.ko

可以通过一下命令判断是否安装成功。
> lsmod | grep hados_doe

### 应用程序编译
**头文件目录**
在`make`或者`cmake`中指定头文件目录`/opt/yusur/hados/include`

**链接SDK**
在`make`或者`cmake`中指定动态库`libhados_doe.so`

# 调试手册
## 调试工具 doe_tool
`doe_tool`是`DOE`调试工具，常被用来做功能验证。通过`-h`命令查看命令如何使用。

> sudo ./doe_tool --help
Usage: doe_tool [OPTION...]
DOE test tool, Operation Code:
&emsp;	0x01 RAW command
&emsp;	0x10 Create array
&emsp;	0x11 Delete array
&emsp;	0x12 Create hash
&emsp;	0x13 Delete hash
&emsp;	0x20 Array load
&emsp;	0x21 Array store
&emsp;	0x24 Array load batch
&emsp;	0x25 Array store batch
&emsp;	0x26 Array read-clear
&emsp;	0x2a Array read-clear batch
&emsp;	0x30 Hash insert
&emsp;	0x31 Hash delete
&emsp;	0x32 Hash query
&emsp;	0x34 Hash insert batch
&emsp;	0x35 Hash delete batch
&emsp;	0x36 Hash query batch
>
> -c, --count_batch=Count    Count of sub cmd for batch
  -d, --depth=Size           Depth of Array/Hash table
  -e, --elem_size=Size       Ouput element size for batch
  -i, --index=Dec            Index of Array table
  -I, --in_batch=File name   Input batch file
  -k, --key=Hex array        Key of Hash table
  -K, --key_len=Size         Key of Hash table
  -o, --opcode=Hex           DOE operation code
  -O, --out_batch=File name  Ouput batch file(read result)
  -t, --tableid=Dec          Target table ID
  -v, --dov=Hex array        Data/Value of Array/Hash table
  -V, --dov_len=Size         Data/Value of Array/Hash table
  -?, --help                 Give this help list
&emsp;     --usage                Give a short usage message
>
>Mandatory or optional arguments to long options are also mandatory or optional
>for any corresponding short options.

### 数组表命令示例
```shell
# create
./doe_tool -o 0x10 -t 127 -d 32 -V 64

# store init value
./doe_tool -o 0x21 -t 127 -i 0 -v "11 22 33 44 55 66 77 88 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 88 77 66 55 44 33 22 11"
./doe_tool -o 0x21 -t 127 -i 2 -v "aa aa aa aa aa aa aa aa 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 88 77 66 55 44 33 22 11"
./doe_tool -o 0x21 -t 127 -i 4 -v "bb bb bb bb bb bb bb bb 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 88 77 66 55 44 33 22 11"
./doe_tool -o 0x21 -t 127 -i 6 -v "cc cc cc cc cc cc cc cc 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 88 77 66 55 44 33 22 11"

# read clear test
./doe_tool -o 0x20 -t 127 -i 0 -V 64
./doe_tool -o 0x26 -t 127 -i 0 -V 64

# single load test
./doe_tool -o 0x20 -t 127 -i 0 -V 64
./doe_tool -o 0x20 -t 127 -i 2 -V 64
./doe_tool -o 0x20 -t 127 -i 4 -V 64
./doe_tool -o 0x20 -t 127 -i 6 -V 64

echo -ne "\\x00\\x00\\x00\\x00\\x02\\x00\\x00\\x00\\x04\\x00\\x00\\x00\\x06\\x00\\x00\\x00" > index_list
# batch load test
./doe_tool -o 0x24 -t 127 -I index_list -O pair_list -e 68 -c 4

# batch read clear test
./doe_tool -o 0x2a -t 127 -I index_list -O pair_list -e 68 -c 4

# batch store test
./doe_tool -o 0x25 -t 127 -I pair_list -c 4
./doe_tool -o 0x20 -t 127 -i 0 -V 64
./doe_tool -o 0x20 -t 127 -i 2 -V 64
./doe_tool -o 0x20 -t 127 -i 4 -V 64
./doe_tool -o 0x20 -t 127 -i 6 -V 64

# delete table
./doe_tool -o 0x11 -t 127

```

### 哈希表命令示例

```shell
# create
./doe_tool -o 0x12 -t 119 -d 32 -K 32 -V 8

# insert & query & delete
./doe_tool -o 0x32 -t 119 -k "00 aa bb cc dd ee ff 34 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ff" -V 8
./doe_tool -o 0x30 -t 119 -k "00 aa bb cc dd ee ff 34 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ff" -v "01 02 03 04 05 06 07 08"
./doe_tool -o 0x32 -t 119 -k "00 aa bb cc dd ee ff 34 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ff" -V 8
./doe_tool -o 0x31 -t 119 -k "00 aa bb cc dd ee ff 34 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ff"

# insert multi
./doe_tool -o 0x30 -t 119 -k "00 aa bb cc dd ee ff 34 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ff" -v "00 f2 f3 f4 f5 f6 f7 f8"
./doe_tool -o 0x30 -t 119 -k "01 aa bb cc dd ee ff 34 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ff" -v "01 f2 f3 f4 f5 f6 f7 f8"
./doe_tool -o 0x30 -t 119 -k "02 aa bb cc dd ee ff 34 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ff" -v "02 f2 f3 f4 f5 f6 f7 f8"
./doe_tool -o 0x30 -t 119 -k "03 aa bb cc dd ee ff 34 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ff" -v "03 f2 f3 f4 f5 f6 f7 f8"

./doe_tool -o 0x32 -t 119 -k "00 aa bb cc dd ee ff 34 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ff" -V 8
./doe_tool -o 0x32 -t 119 -k "01 aa bb cc dd ee ff 34 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ff" -V 8
./doe_tool -o 0x32 -t 119 -k "02 aa bb cc dd ee ff 34 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ff" -V 8
./doe_tool -o 0x32 -t 119 -k "03 aa bb cc dd ee ff 34 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ff" -V 8

# batch query test
echo -ne "\\x00\\xaa\\xbb\\xcc\\xdd\\xee\\xff\\x34\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\xff" > key_list
echo -ne "\\x01\\xaa\\xbb\\xcc\\xdd\\xee\\xff\\x34\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\xff" >> key_list
echo -ne "\\x02\\xaa\\xbb\\xcc\\xdd\\xee\\xff\\x34\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\xff" >> key_list
echo -ne "\\x03\\xaa\\xbb\\xcc\\xdd\\xee\\xff\\x34\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\xff" >> key_list
./doe_tool -o 0x36 -t 119 -I key_list -O pair_list -e 40 -c 4

# delete
./doe_tool -o 0x13 -t 119
```
